FROM python:3.10

RUN apt-get update
RUN apt-get install ffmpeg libsm6 libxext6 -y

WORKDIR /app

COPY . /app
RUN pip install --no-cache-dir .

CMD [ "python", "run.py" ]
