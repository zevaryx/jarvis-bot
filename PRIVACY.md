# Privacy Policy
Your privacy is important to us. It is JARVIS' policy to respect your privacy and comply with any applicable law and regulation regarding any personal information we may collect about you through our JARVIS bot.

This policy is effective as of 20 March 2022 and was last updated on 10 March 2022.

## Information We Collect
Information we collect includes both information you knowingly and actively provide us when using or participating in any of our services and promotions, and any information automatically sent by your devices in the course of accessing our products and services.

## Log Data
When you use our JARVIS services, if opted in to usage data collection services, we may collect data in certain cicumstances:

- Administrative activity (i.e. ban, warn, mute)
  - Your User ID (either as admin or as the recipient of the activity)
  - Guild ID of activity origin
  - Your discriminator at time of activity (bans only)
  - Your username at time of activity (bans only)
- Admin commands
  - User ID of admin who executes admin command
- Reminders
  - Your User ID
  - The guild in which the command originated
  - The channel in which the command originated
  - Private text entered via the command
- Starboard
  - Message ID of starred message
  - Channel ID of starred message
  - Guild ID of origin guild
- Automated activity logging
  - We store no information about users who edit nicknames, join/leave servers, or other related activities. However, this information, if configured by server admins, is relayed into a Discord channel and is not automatically deleted, nor do we have control over this information.
    - This information is also stored by Discord via their Audit Log, which we also have no control over. Please contact Discord for their own privacy policy and asking about your rights on their platform.

## Use of Information
We use the information we collect to provide, maintain, and improve our services. Common uses where this data may be used includes sending reminders, helping administrate Discord servers, and providing extra utilities into Discord based on user content.

## Security of Your Personal Information
Although we will do our best to protect the personal information you provide to us, we advise that no method of electronic transmission or storage is 100% secure, and no one can guarantee absolute data security. We will comply with laws applicable to us in respect of any data breach.

## How Long We Keep Your Personal Information
We keep your personal information only for as long as we need to. This time period may depend on what we are using your information for, in accordance with this privacy policy. If your personal information is no longer required, we will delete it or make it anonymous by removing all details that identify you.

## Your Rights and Controlling Your Personal Information
You may request access to your personal information, and change what you are okay with us collecting from you. You may also request that we delete your personal identifying information. Please message **zevaryx#5779** on Discord, or join the Discord server at https://discord.gg/4TuFvW5n and ask in there.

## Limits of Our Policy
Our website may link to external sites that are not operated by us (ie. discord.com). Please be aware that we have no control over the content and policies of those sites, and cannot accept responsibility or liability for their respective privacy practices.

## Changes to This Policy
At our discretion, we may change our privacy policy to reflect updates to our business processes, current acceptable practices, or legislative or regulatory changes. If we decide to change this privacy policy, we will post the changes here at the same link by which you are accessing this privacy policy.

## Contact Us
For any questions or concerns regarding your privacy, you may contact us using the following details:

### Discord
#### zevaryx#5779
#### https://discord.gg/4TuFvW5n
