<div align="center">
    <img width=15% alt="JARVIS" src="https://git.zevaryx.com/stark-industries/jarvis/jarvis-bot/-/raw/main/jarvis_small.png">  

# Just Another Rather Very Intelligent System

<br />

[![python 3.10+](https://img.shields.io/badge/python-3.10+-blue)]()
[![tokei lines of code](http://135.148.148.80:8000/b1/gitlab/zevaryx/jarvis-bot?category=code)](https://git.zevaryx.com/stark-industries/jarvis/jarvis-bot)
[![discord chat widget](https://img.shields.io/discord/862402786116763668?style=social&logo=discord)](https://discord.gg/VtgZntXcnZ)

[![ko-fi](https://www.ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/zevaryx)
</div>

Welcome to the JARVIS Initiative, an open-source multi-purpose bot

## Features

JARVIS currently offers:

- 👩‍💼 **Administration**: `verify`, `ban/unban`, `kick`, `purge`, `mute/unmute` and more!
- 🚓 **Moderation**: `lock/unlock`, `lockdown`, `warn`, `autoreact`, and also more!
- 🔧 **Utilities**: `remindme`, `rolegiver`, `temprole`, `image`, and so many more!
- 🏷️ **Tags**: Custom `tag`s! Useful for custom messages without the hassle!

## Contributing

Before **creating an issue**, please ensure that it hasn't already been reported/suggested.
If you have a question, please join the community Discord before opening an issue.

If you wish to contribute to the JARVIS codebase or documentation, join the Discord! The recognized developers there will help you get started.

## Community

Join the [Stark R&D Department Discord server](https://discord.gg/VtgZntXcnZ) to be kept up-to-date on code updates and issues.

## Requirements

- MongoDB 6.0 or higher
- Python 3.10 or higher
- [tokei](https://github.com/XAMPPRocky/tokei) 12.1 or higher
- Everything in `requirements.txt`

## JARVIS Cogs

Current cogs that are implemented:

- `AdminCog`
  - Handles all admin commands
- `AutoreactCog`
  - Handles autoreaction configuration
- `BotutilCog`
  - Handles internal bot utilities (private use only)
- `DevCog`
  - Developer utilities, such as hashing, encoding, and UUID generation
- `GitlabCog`
  - Shows Gitlab information about J.A.R.V.I.S.
- `ImageCog`
  - Image-processing cog.
- `RedditCog`
  - Reddit lookup and following commands
- `RemindmeCog`
  - Manage reminders
- `RolegiverCog`
  - Configure selectable roles
- `SettingsCog`
  - Manage Guild settings
- `StarboardCog`
  - Configure and add starboards and stars
- `TagCog`
  - For managing tags
- `TemproleCog`
  - For managing and assigning temporary roles
- `TwitterCog`
  - Twitter following commands
- `UtilCog`
  - Generic utilities, like userinfo and roleinfo
- `VerifyCog`
  - Guild verification

## Directories

### `jarvis`

The bot itself

#### `jarvis.cogs`

All of the cogs listed above are stored in this directory

##### `jarvis.cogs.admin`

Contains all AdminCogs, including:

- `BanCog`
- `KickCog`
- `LockCog`
- `LockdownCog`
- `ModcaseCog`
- `MuteCog`
- `PurgeCog`
- `RolepingCog`
- `WarningCog`

#### `jarvis.data`

Contains data relevant to J.A.R.V.I.S., such as emoji lookups and dbrand data

##### `jarvis.data.json`

Any JSON files that are needed are stored here

#### `jarvis.utils`

Generic utilities
