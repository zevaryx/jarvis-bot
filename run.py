"""Main run file for J.A.R.V.I.S."""
import nest_asyncio

nest_asyncio.apply()

import asyncio

import nest_asyncio

from jarvis import run

nest_asyncio.apply()

if __name__ == "__main__":
    asyncio.run(run())
