"""Permissions wrappers."""
from interactions import InteractionContext, Permissions


def admin_or_permissions(*perms: list) -> bool:
    """Check if a user is an admin or has other perms."""

    async def predicate(ctx: InteractionContext) -> bool:
        """Extended check predicate."""  # noqa: D401
        is_admin = ctx.author.has_permission(Permissions.ADMINISTRATOR)
        has_other = any(ctx.author.has_permission(perm) for perm in perms)
        return is_admin or has_other

    return predicate
