"""JARVIS Utility Functions."""
from datetime import datetime, timezone
from pkgutil import iter_modules

import git
from interactions.models.discord.embed import Embed, EmbedField
from interactions.models.discord.guild import AuditLogEntry
from interactions.models.discord.user import Member

from jarvis.branding import PRIMARY_COLOR


def build_embed(
    title: str,
    description: str,
    fields: list,
    color: str = PRIMARY_COLOR,
    timestamp: datetime = None,
    **kwargs: dict,
) -> Embed:
    """Embed builder utility function."""
    if not timestamp:
        timestamp = datetime.now(tz=timezone.utc)
    embed = Embed(
        title=title,
        description=description,
        color=color,
        timestamp=timestamp,
        **kwargs,
    )
    for field in fields:
        embed.add_field(**field.to_dict())
    return embed


def modlog_embed(
    member: Member,
    admin: Member,
    title: str,
    desc: str,
    log: AuditLogEntry = None,
    reason: str = None,
) -> Embed:
    """Get modlog embed."""
    fields = [
        EmbedField(
            name="Moderator",
            value=f"{admin.mention} ({admin.username})",
        ),
    ]
    if log and log.reason:
        reason = reason or log.reason
    fields.append(EmbedField(name="Reason", value=reason, inline=False))
    embed = build_embed(
        title=title,
        description=desc,
        color="#fc9e3f",
        fields=fields,
        timestamp=log.created_at,
    )
    embed.set_author(name=f"{member.username}", icon_url=member.display_avatar.url)
    embed.set_footer(text=f"{member.username} | {member.id}")
    return embed


def get_extensions(path: str) -> list:
    """Get JARVIS cogs."""
    vals = [x.name for x in iter_modules(path)]
    return [f"jarvis.cogs.{x}" for x in vals]


def update() -> int:
    """JARVIS update utility."""
    repo = git.Repo(".")
    dirty = repo.is_dirty()
    current_hash = repo.head.object.hexsha
    origin = repo.remotes.origin
    origin.fetch()
    if current_hash != origin.refs["main"].object.hexsha:
        if dirty:
            return 2
        origin.pull()
        return 0
    return 1


def get_repo_hash() -> str:
    """JARVIS current branch hash."""
    repo = git.Repo(".")
    return repo.head.object.hexsha
