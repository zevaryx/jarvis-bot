"""JARVIS error handling mixin."""
import traceback
from datetime import datetime, timezone

from interactions import listen
from interactions.api.events import Error
from interactions.client.errors import (
    CommandCheckFailure,
    CommandOnCooldown,
    HTTPException,
)
from interactions.ext.prefixed_commands.context import PrefixedContext
from interactions.models.internal.context import BaseContext, InteractionContext
from pastypy import AsyncPaste as Paste

DEFAULT_GUILD = 862402786116763668
DEFAULT_ERROR_CHANNEL = 943395824560394250
DEFAULT_SITE = "https://paste.zevs.me"

ERROR_MSG = """
Command Information:
  Guild: {guild_name}
  Name: {invoke_target}
  Args:
{arg_str}

Callback:
  Args:
{callback_args}
  Kwargs:
{callback_kwargs}
"""


class ErrorMixin:
    @listen()
    async def on_error(self, event: Error, *args, **kwargs) -> None:
        """NAFF on_error override."""
        source = event.source
        error = event.error
        if isinstance(error, HTTPException):
            errors = error.search_for_message(error.errors)
            out = (
                f"HTTPException: {error.status}|{error.response.reason}: "
                + "\n".join(errors)
            )
            self.logger.error(out, exc_info=error)
        else:
            self.logger.error(f"Ignoring exception in {source}", exc_info=error)

    async def on_command_error(
        self, ctx: BaseContext, error: Exception, *args: list, **kwargs: dict
    ) -> None:
        """NAFF on_command_error override."""
        name = ctx.invoke_target
        self.logger.debug(f"Handling error in {name}: {error}")
        if isinstance(error, CommandOnCooldown):
            await ctx.send(str(error), ephemeral=True)
            return
        elif isinstance(error, CommandCheckFailure):
            await ctx.send("I'm afraid I can't let you do that", ephemeral=True)
            return
        guild = await self.fetch_guild(DEFAULT_GUILD)
        channel = await guild.fetch_channel(DEFAULT_ERROR_CHANNEL)
        error_time = datetime.now(tz=timezone.utc).strftime("%d-%m-%Y %H:%M-%S.%f UTC")
        timestamp = int(datetime.now(tz=timezone.utc).timestamp())
        timestamp = f"<t:{timestamp}:T>"
        arg_str = ""
        if isinstance(ctx, InteractionContext) and ctx.target_id:
            ctx.kwargs["context target"] = ctx.target
        if isinstance(ctx, InteractionContext):
            for k, v in ctx.kwargs.items():
                arg_str += f"    {k}: "
                if isinstance(v, str) and len(v) > 100:
                    v = v[97] + "..."
                arg_str += f"{v}\n"
        elif isinstance(ctx, PrefixedContext):
            for v in ctx.args:
                if isinstance(v, str) and len(v) > 100:
                    v = v[97] + "..."
                arg_str += f"    - {v}"
        callback_args = "\n".join(f"    - {i}" for i in args) if args else "    None"
        callback_kwargs = (
            "\n".join(f"    {k}: {v}" for k, v in kwargs.items())
            if kwargs
            else "    None"
        )
        full_message = ERROR_MSG.format(
            guild_name=ctx.guild.name,
            error_time=error_time,
            invoke_target=name,
            arg_str=arg_str,
            callback_args=callback_args,
            callback_kwargs=callback_kwargs,
        )
        tb = traceback.format_exception(error)
        if isinstance(error, HTTPException):
            errors = error.search_for_message(error.errors)
            tb[
                -1
            ] = f"HTTPException: {error.status}|{error.response.reason}: " + "\n".join(
                errors
            )
        error_message = "".join(traceback.format_exception(error))
        if len(full_message + error_message) >= 1800:
            error_message = "\n  ".join(error_message.split("\n"))
            full_message += "Exception: |\n  " + error_message
            paste = Paste(content=full_message, site=DEFAULT_SITE)
            key = await paste.save()
            self.logger.debug(f"Large traceback, saved to Pasty {paste.id}, {key=}")

            await channel.send(
                f"JARVIS encountered an error at {timestamp}. Log too big to send over Discord."
                f"\nPlease see log at {paste.url}"
            )
        else:
            await channel.send(
                f"JARVIS encountered an error at {timestamp}:"
                f"\n```yaml\n{full_message}\n```"
                f"\nException:\n```py\n{error_message}\n```"
            )
        await ctx.send(
            "Whoops! Encountered an error. The error has been logged.", ephemeral=True
        )
        try:
            await ctx.defer(ephemeral=True)
            return await super().on_command_error(ctx, error, *args, **kwargs)
        except Exception as e:
            self.logger.error("Uncaught exception", exc_info=e)
