"""JARVIS member event mixin."""
import asyncio

from interactions import listen
from interactions.api.events.discord import MemberAdd, MemberRemove, MemberUpdate
from interactions.client.utils.misc_utils import get
from interactions.models.discord.embed import Embed, EmbedField
from interactions.models.discord.enums import AuditLogEventType
from interactions.models.discord.user import Member
from jarvis_core.db.models import Setting, Temprole

from jarvis.utils import build_embed


class MemberEventMixin:
    # Events
    # Member
    @listen()
    async def on_member_add(self, event: MemberAdd) -> None:
        """Handle on_member_add event."""
        user = event.member
        guild = event.guild
        unverified = await Setting.find_one(
            Setting.guild == guild.id, Setting.setting == "unverified"
        )
        temproles = await Temprole.find(
            Temprole.guild == guild.id, Temprole.user == user.id
        ).to_list()
        if unverified:
            self.logger.debug(f"Applying unverified role to {user.id} in {guild.id}")
            role = await guild.fetch_role(unverified.value)
            if not role:
                self.logger.warn(
                    f"Unverified role no longer exists for guild {guild.id}"
                )
                await settings.delete()
            elif role not in user.roles:
                await user.add_role(role, reason="User just joined and is unverified")
        if temproles:
            self.logger.debug(f"User {user.id} still has temprole(s) in {guild.id}")
            for temprole in temproles:
                role = await guild.fetch_role(temprole.role)
                if not role:
                    await temprole.delete()
                elif role not in user.roles:
                    await user.add_role(
                        role, reason="User joined and has existing temprole"
                    )

    @listen()
    async def on_member_remove(self, event: MemberRemove) -> None:
        """Handle on_member_remove event."""
        user = event.member
        guild = event.guild
        log = await Setting.find_one(
            Setting.guild == guild.id, Setting.setting == "activitylog"
        )
        if log:
            self.logger.debug(f"User {user.id} left {guild.id}")
            channel = await guild.fetch_channel(log.value)
            embed = build_embed(
                title="Member Left",
                description=f"{user.username} left {guild.name}",
                fields=[],
            )
            embed.set_author(name=user.username, icon_url=user.avatar.url)
            embed.set_footer(text=f"{user.username} | {user.id}")
            await channel.send(embeds=embed)

    async def process_verify(self, before: Member, after: Member) -> Embed:
        """Process user verification."""
        auditlog = await after.guild.fetch_audit_log(
            user_id=before.id, action_type=AuditLogEventType.MEMBER_ROLE_UPDATE
        )
        audit_event = get(auditlog.events, reason="Verification passed")
        if audit_event:
            admin_mention = "[N/A]"
            admin_text = "[N/A]"
            if admin := await after.guild.fet_member(audit_event.user_id):
                admin_mention = admin.mention
                admin_text = f"{admin.username}"
            fields = (
                EmbedField(name="Moderator", value=f"{admin_mention} ({admin_text})"),
                EmbedField(name="Reason", value=audit_event.reason),
            )
            embed = build_embed(
                title="User Verified",
                description=f"{after.mention} was verified",
                fields=fields,
            )
            embed.set_author(name=after.display_name, icon_url=after.display_avatar.url)
            embed.set_footer(text=f"{after.username} | {after.id}")
            return embed

    async def process_rolechange(self, before: Member, after: Member) -> Embed:
        """Process role changes."""
        if before.roles == after.roles:
            return

        new_roles = []
        removed_roles = []

        for role in before.roles:
            if role not in after.roles:
                removed_roles.append(role)
        for role in after.roles:
            if role not in before.roles:
                new_roles.append(role)

        new_text = "\n".join(role.mention for role in new_roles) or "None"
        removed_text = "\n".join(role.mention for role in removed_roles) or "None"

        fields = (
            EmbedField(name="Added Roles", value=new_text),
            EmbedField(name="Removed Roles", value=removed_text),
        )
        embed = build_embed(
            title="User Roles Changed",
            description=f"{after.mention} had roles changed",
            fields=fields,
        )
        embed.set_author(name=after.display_name, icon_url=after.display_avatar.url)
        embed.set_footer(text=f"{after.username} | {after.id}")
        return embed

    async def process_rename(self, before: Member, after: Member) -> None:
        """Process name change."""
        if before.nickname == after.nickname and before.username == after.username:
            return

        fields = (
            EmbedField(
                name="Before",
                value=f"{before.display_name} ({before.username})",
            ),
            EmbedField(name="After", value=f"{after.display_name} ({after.username})"),
        )
        embed = build_embed(
            title="User Renamed",
            description=f"{after.mention} changed their name",
            fields=fields,
            color="#fc9e3f",
        )
        embed.set_author(name=after.display_name, icon_url=after.display_avatar.url)
        embed.set_footer(text=f"{after.username} | {after.id}")
        return embed

    @listen()
    async def on_member_update(self, event: MemberUpdate) -> None:
        """Handle on_member_update event."""
        before = event.before
        after = event.after

        if (
            before.display_name == after.display_name and before.roles == after.roles
        ) or (not after or not before):
            return

        log = await Setting.find_one(
            Setting.guild == before.guild.id, Setting.setting == "activitylog"
        )
        if log:
            channel = await before.guild.fetch_channel(log.value)
            await asyncio.sleep(0.5)  # Wait for audit log
            embed = None
            if before._role_ids != after._role_ids:
                verified = await Setting.find_one(
                    Setting.guild == before.guild.id, Setting.setting == "verified"
                )
                v_role = None
                if verified:
                    v_role = await before.guild.fetch_role(verified.value)
                    if not v_role:
                        self.logger.debug(
                            f"Guild {before.guild.id} verified role no longer exists"
                        )
                        await verified.delete()
                    else:
                        if not before.has_role(v_role) and after.has_role(v_role):
                            embed = await self.process_verify(before, after)
                    embed = embed or await self.process_rolechange(before, after)
            embed = embed or await self.process_rename(before, after)
            if embed:
                await channel.send(embeds=embed)
