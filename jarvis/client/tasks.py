"""JARVIS task mixin."""
from aiohttp import ClientSession
from interactions.models.internal.tasks.task import Task
from interactions.models.internal.tasks.triggers import IntervalTrigger


class TaskMixin:
    @Task.create(IntervalTrigger(minutes=1))
    async def _update_domains(self) -> None:
        async with ClientSession(
            headers={"X-Identity": "Discord: zevaryx#5779"}
        ) as session:
            response = await session.get("https://phish.sinking.yachts/v2/recent/60")
            response.raise_for_status()
            data = await response.json()

        if len(data) == 0:
            return
        self.logger.debug(f"Found {len(data)} changes to phishing domains")

        add = 0
        sub = 0

        for update in data:
            if update["type"] == "add":
                for domain in update["domains"]:
                    if domain not in self.phishing_domains:
                        add += 1
                        self.phishing_domains.append(domain)
            elif update["type"] == "delete":
                for domain in update["domains"]:
                    if domain in self.phishing_domains:
                        sub -= 1
                        self.phishing_domains.remove(domain)
        self.logger.info(f"[antiphish] {add} additions, {sub} removals")

    @Task.create(IntervalTrigger(minutes=30))
    async def _update_currencies(self) -> None:
        await self.erapi.update_async()
