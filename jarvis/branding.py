"""JARVIS brandings."""
PRIMARY_COLOR = "#3498db"
HARD_ACTION = "#ff0000"
MODERATE_ACTION = "#ff9900"
SOFT_ACTION = "#ffff00"
GOOD_ACTION = "#00ff00"

COMMAND_TYPES = {
    "HARD": ["ban", "lockdown"],
    "MODERATE": ["kick", "mute", "lock"],
    "SOFT": ["warning"],
    "GOOD": ["unban", "unmute"],
}
CUSTOM_COMMANDS = {}
CUSTOM_EMOJIS = {
    "ico_clock_green": "<:ico_clock_green:1019710693206933605>",
    "ico_clock_yellow": "<:ico_clock_yellow:1019710734340472834>",
    "ico_clock_red": "<:ico_clock_red:1019710735896551534>",
    "ico_check_green": "<:ico_check_green:1019725504120639549>",
}


def get_command_color(command: str) -> str:
    """
    Get a command's color.

    Args:
        command: Command name

    Returns:
        Hex color string
    """
    if command in COMMAND_TYPES["HARD"]:
        return HARD_ACTION
    elif command in COMMAND_TYPES["MODERATE"]:
        return MODERATE_ACTION
    elif command in COMMAND_TYPES["SOFT"]:
        return SOFT_ACTION
    elif command in COMMAND_TYPES["GOOD"]:
        return GOOD_ACTION
    else:
        return CUSTOM_COMMANDS.get(command, PRIMARY_COLOR)
