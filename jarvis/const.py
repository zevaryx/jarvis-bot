"""JARVIS constants."""
from importlib.metadata import version as _v

try:
    __version__ = _v("jarvis")
except Exception:
    __version__ = "0.0.0"
