"""JARVIS bot-specific embeds."""
from typing import Optional

from interactions.models.discord.embed import Embed, EmbedField
from interactions.models.discord.guild import Guild
from interactions.models.discord.user import Member, User

from jarvis.branding import get_command_color
from jarvis.utils import build_embed


def ban_embed(
    user: User,
    admin: User,
    reason: str,
    type: str,
    guild: Guild,
    duration: Optional[int] = None,
    dm: bool = False,
) -> Embed:
    """
    Generate a ban embed.

    Args:
        user: User to ban
        admin: Admin to execute ban
        reason: Reason for ban
        type: Ban type
        guild: Guild to ban from
        duration: Optional temporary ban duration
        dm: If the embed should be a user embed
    """
    fields = [EmbedField(name="Reason", value=reason), EmbedField(name="Type", value=type)]
    if duration:
        fields.append(EmbedField(name="Duration", value=f"{duration} hours"))
    fields.append(EmbedField(name="Admin", value=f"{admin.username}#{admin.discriminator} ({admin.mention})"))
    if dm:
        embed = build_embed(
            title=f"You have been banned from {guild.name}",
            description=None,
            fields=fields,
            color=get_command_color("ban"),
        )
        embed.set_thumbnail(url=guild.icon.url)
    else:
        embed = build_embed(
            title="User Banned",
            description=f"{user.mention} has been banned",
            fields=fields,
            color=get_command_color("ban"),
        )
        embed.set_thumbnail(url=user.display_avatar.url)
    embed.set_author(name=user.display_name, icon_url=user.display_avatar.url)
    embed.set_footer(text=f"{user.username}#{user.discriminator} | {user.id}")

    return embed


def unban_embed(user: User, admin: User, reason: str) -> Embed:
    """
    Generate an unban embed.

    Args:
        user: User to unban
        admin: Admin to execute unban
        reason: Reason for unban
    """
    fields = (
        EmbedField(name="Reason", value=reason),
        EmbedField(name="Admin", value=f"{admin.username}#{admin.discriminator} ({admin.mention})"),
    )
    embed = build_embed(
        title="User Unbanned",
        description=f"{user.mention} was unbanned",
        fields=fields,
        color=get_command_color("unban"),
    )
    embed.set_thumbnail(url=user.display_avatar.url)
    embed.set_author(name=user.display_name, icon_url=user.display_avatar.url)
    embed.set_footer(text=f"{user.username}#{user.discriminator} | {user.id}")

    return embed


def kick_embed(user: Member, admin: Member, reason: str, guild: Guild, dm: bool = False) -> Embed:
    """
    Generate a kick embed.

    Args:
        user: User to kick
        admin: Admin to execute kick
        reason: Reason for kick
        guild: Guild to kick from
        dm: If the embed should be a user embed
    """
    fields = [
        EmbedField(name="Reason", value="A valid reason"),
        EmbedField(
            name="Admin",
            value=f"{admin.username}#{admin.discriminator} ({admin.mention})",
        ),
    ]
    if dm:
        embed = build_embed(
            title=f"You have been kicked from {guild.name}",
            color=get_command_color("kick"),
            fields=fields,
        )
        embed.set_thumbnail(url=guild.icon.url)
    else:
        embed = build_embed(
            title="User Kicked",
            description=f"{user.mention} has been kicked",
            fields=fields,
            color=get_command_color("kick"),
        )

    embed.set_author(name=user.display_name, icon_url=user.display_avatar.url)
    embed.set_footer(text=f"{user.username}#{user.discriminator} | {user.id}")

    return embed


def mute_embed(user: Member, admin: Member, reason: str, guild: Guild) -> Embed:
    """
    Generate a mute embed.

    Args:
        user: User to mute
        admin: Admin to execute mute
        reason: Reason for mute
    """
    until = int(user.communication_disabled_until.timestamp())
    fields = (
        EmbedField(name="Reason", value=reason),
        EmbedField(name="Until", value=f"<t:{until}:F> (<t:{until}:R>)"),
        EmbedField(
            name="Admin",
            value=f"{admin.username}#{admin.discriminator} ({admin.mention})",
        ),
    )
    embed = build_embed(
        title="User Muted",
        description=f"{user.mention} has been muted",
        fields=fields,
        color=get_command_color("mute"),
    )

    embed.set_thumbnail(url=user.display_avatar.url)
    embed.set_author(name=user.display_name, icon_url=user.display_avatar.url)
    embed.set_footer(text=f"{user.username}#{user.discriminator} | {user.id}")

    return embed


def unmute_embed(user: Member, admin: Member, reason: str, guild: Guild) -> Embed:
    """
    Generate an umute embed.

    Args:
        user: User to unmute
        admin: Admin to execute unmute
        reason: Reason for unmute
    """
    fields = (
        EmbedField(name="Reason", value=reason),
        EmbedField(
            name="Admin",
            value=f"{admin.username}#{admin.discriminator} ({admin.mention})",
        ),
    )
    embed = build_embed(
        title="User Unmuted",
        description=f"{user.mention} has been unmuted",
        fields=fields,
        color=get_command_color("mute"),
    )

    embed.set_thumbnail(url=user.display_avatar.url)
    embed.set_author(name=user.display_name, icon_url=user.display_avatar.url)
    embed.set_footer(text=f"{user.username}#{user.discriminator} | {user.id}")

    return embed


def warning_embed(user: Member, reason: str, admin: Member) -> Embed:
    """
    Generate a warning embed.

    Args:
        user: User to warn
        reason: Warning reason
        admin: Admin who sent the warning
    """
    fields = (
        EmbedField(name="Reason", value=reason, inline=False),
        EmbedField(name="Admin", value=f"{admin.username}#{admin.discriminator} ({admin.mention})"),
    )
    embed = build_embed(
        title="Warning",
        description=f"{user.mention} has been warned",
        fields=fields,
        color=get_command_color("warning"),
    )
    embed.set_author(name=user.display_name, icon_url=user.display_avatar.url)
    embed.set_footer(text=f"{user.username}#{user.discriminator} | {user.id}")
    return embed
