"""Load the config for JARVIS"""
from enum import Enum
from os import environ
from pathlib import Path
from typing import Optional

import orjson as json
import yaml
from dotenv import load_dotenv
from jarvis_core.util import find_all
from pydantic import BaseModel

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader


class Mongo(BaseModel):
    """MongoDB config."""

    host: list[str] | str = "localhost"
    username: Optional[str] = None
    password: Optional[str] = None
    port: int = 27017


class Redis(BaseModel):
    """Redis config."""

    host: str = "localhost"
    username: Optional[str] = None
    password: Optional[str] = None


class Mastodon(BaseModel):
    """Mastodon config."""

    token: str
    url: str


class Environment(Enum):
    """JARVIS running environment."""

    production = "production"
    develop = "develop"


class Config(BaseModel):
    """JARVIS config model."""

    token: str
    """Bot token"""
    erapi: str
    """exchangerate-api.org API token"""
    environment: Environment = Environment.develop
    mongo: Mongo
    redis: Redis
    mastodon: Optional[Mastodon] = None
    urls: Optional[dict[str, str]] = None
    sync: bool = False
    log_level: str = "INFO"
    jurigged: bool = False


_config: Config = None


def _load_json() -> Config | None:
    path = Path("config.json")
    config = None
    if path.exists():
        with path.open() as f:
            j = json.loads(f.read())
            config = Config(**j)

    return config


def _load_yaml() -> Config | None:
    path = Path("config.yaml")
    config = None
    if path.exists():
        with path.open() as f:
            y = yaml.load(f.read(), Loader=Loader)
            config = Config(**y)

    return config


def _load_env() -> Config | None:
    load_dotenv()
    data = {}
    mongo = {}
    redis = {}
    mastodon = {}
    urls = {}
    mongo_keys = find_all(lambda x: x.upper().startswith("MONGO"), environ.keys())
    redis_keys = find_all(lambda x: x.upper().startswith("REDIS"), environ.keys())
    mastodon_keys = find_all(lambda x: x.upper().startswith("MASTODON"), environ.keys())
    url_keys = find_all(lambda x: x.upper().startswith("URLS"), environ.keys())

    config_keys = (
        mongo_keys
        + redis_keys
        + mastodon_keys
        + url_keys
        + ["TOKEN", "SYNC", "LOG_LEVEL", "JURIGGED"]
    )

    for item, value in environ.items():
        if item not in config_keys:
            continue

        if item in mongo_keys:
            key = "_".join(item.split("_")[1:]).lower()
            mongo[key] = value
        elif item in redis_keys:
            key = "_".join(item.split("_")[1:]).lower()
            redis[key] = value
        elif item in mastodon_keys:
            key = "_".join(item.split("_")[1:]).lower()
            mastodon[key] = value
        elif item in url_keys:
            key = "_".join(item.split("_")[1:]).lower()
            urls[key] = value
        else:
            if item == "SYNC":
                value = value.lower() in ["yes", "true"]
            data[item.lower()] = value

    data["mongo"] = mongo
    data["redis"] = redis
    if all(x is not None for x in mastodon.values()):
        data["mastodon"] = mastodon
    data["urls"] = {k: v for k, v in urls if v}

    return Config(**data)


def load_config(method: Optional[str] = None) -> Config:
    """
    Load the config using the specified method first

    Args:
        method: Method to use first
    """
    global _config
    if _config is not None:
        return _config

    methods = {"yaml": _load_yaml, "json": _load_json, "env": _load_env}
    method_names = list(methods.keys())
    if method and method in method_names:
        method_names.remove(method)
        method_names.insert(0, method)

    for method in method_names:
        if _config := methods[method]():
            return _config

    raise FileNotFoundError("Missing one of: config.yaml, config.json, .env")
