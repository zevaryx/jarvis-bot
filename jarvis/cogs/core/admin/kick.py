"""JARVIS KickCog."""
from interactions import InteractionContext, Permissions
from interactions.models.discord.user import User
from interactions.models.internal.application_commands import (
    OptionType,
    slash_command,
    slash_option,
)
from interactions.models.internal.command import check
from jarvis_core.db.models import Kick

from jarvis.embeds.admin import kick_embed
from jarvis.utils.cogs import ModcaseCog
from jarvis.utils.permissions import admin_or_permissions


class KickCog(ModcaseCog):
    """JARVIS KickCog."""

    @slash_command(name="kick", description="Kick a user")
    @slash_option(name="user", description="User to kick", opt_type=OptionType.USER, required=True)
    @slash_option(name="reason", description="Kick reason", opt_type=OptionType.STRING, required=True)
    @check(admin_or_permissions(Permissions.BAN_MEMBERS))
    async def _kick(self, ctx: InteractionContext, user: User, reason: str) -> None:
        if not user or user == ctx.author:
            await ctx.send("You cannot kick yourself.", ephemeral=True)
            return
        if user == self.bot.user:
            await ctx.send("I'm afraid I can't let you do that", ephemeral=True)
            return
        if len(reason) > 100:
            await ctx.send("Reason must be < 100 characters", ephemeral=True)
            return
        if not await ctx.guild.fetch_member(user.id):
            await ctx.send("User must be in guild", ephemeral=True)
            return

        embed = kick_embed(user=user, admin=ctx.author, reason=reason, guild=ctx.guild, dm=True)

        try:
            await user.send(embeds=embed)
        except Exception:
            self.logger.warn(f"Failed to send kick message to {user.id}")
        try:
            await ctx.guild.kick(user, reason=reason)
        except Exception as e:
            await ctx.send(f"Failed to kick user:\n```\n{e}\n```", ephemeral=True)
            return

        embed = kick_embed(user=user, admin=ctx.author, reason=reason, guild=ctx.guild)

        k = Kick(
            user=user.id,
            reason=reason,
            admin=ctx.author.id,
            guild=ctx.guild.id,
        )
        await k.save()
        await ctx.send(embeds=embed)
