"""Filters cog."""
import asyncio
import difflib
import re
from typing import Dict, List

from interactions import (
    AutocompleteContext,
    Client,
    Extension,
    InteractionContext,
    Permissions,
)
from interactions.models.discord.modal import InputText, Modal, TextStyles
from interactions.models.internal.application_commands import (
    OptionType,
    SlashCommand,
    slash_option,
)
from interactions.models.internal.command import check
from jarvis_core.db.models import Filter
from thefuzz import process

from jarvis.utils.permissions import admin_or_permissions


class FilterCog(Extension):
    """JARVIS Filter cog."""

    def __init__(self, bot: Client):
        self.bot = bot
        self.cache: Dict[int, List[str]] = {}

    async def _edit_filter(
        self, ctx: InteractionContext, name: str, search: bool = False
    ) -> None:
        try:
            content = ""
            f: Filter = None
            if search:
                if f := await Filter.find_one(
                    Filter.name == name, Filter.guild == ctx.guild.id
                ):
                    content = "\n".join(f.filters)

            kw = "Updating" if search else "Creating"

            modal = Modal(
                *[
                    InputText(
                        label="Filter (one statement per line)",
                        placeholder="" if content else "i.e. $bad_word^",
                        custom_id="filters",
                        max_length=3000,
                        value=content,
                        style=TextStyles.PARAGRAPH,
                    )
                ],
                title=f'{kw} filter "{name}"',
            )
            await ctx.send_modal(modal)
            try:
                data = await self.bot.wait_for_modal(
                    modal, author=ctx.author.id, timeout=60 * 5
                )
                filters = data.responses.get("filters").split("\n")
            except asyncio.TimeoutError:
                return
            # Thanks, Glitter
            new_name = re.sub(r"[^\w-]", "", name)
            try:
                if not f:
                    f = Filter(name=new_name, guild=ctx.guild.id, filters=filters)
                else:
                    f.name = new_name
                    f.filters = filters
                await f.save()
            except Exception as e:
                await data.send(f"{e}", ephemeral=True)
                return

            content = content.splitlines()
            diff = "\n".join(difflib.ndiff(content, filters)).replace("`", "\u200b`")

            await data.send(
                f"Filter `{new_name}` has been updated:\n\n```diff\n{diff}\n```"
            )

            if ctx.guild.id not in self.cache:
                self.cache[ctx.guild.id] = []
            if new_name not in self.cache[ctx.guild.id]:
                self.cache[ctx.guild.id].append(new_name)
            if name != new_name:
                self.cache[ctx.guild.id].remove(name)
        except Exception as e:
            self.logger.error(e, exc_info=True)

    filter_ = SlashCommand(name="filter", description="Manage keyword filters")

    @filter_.subcommand(
        sub_cmd_name="create", sub_cmd_description="Create a new filter"
    )
    @slash_option(
        name="name",
        description="Name of new filter",
        required=True,
        opt_type=OptionType.STRING,
    )
    @check(admin_or_permissions(Permissions.MANAGE_MESSAGES))
    async def _filter_create(self, ctx: InteractionContext, name: str) -> None:
        return await self._edit_filter(ctx, name)

    @filter_.subcommand(sub_cmd_name="edit", sub_cmd_description="Edit a filter")
    @slash_option(
        name="name",
        description="Filter to edit",
        autocomplete=True,
        opt_type=OptionType.STRING,
        required=True,
    )
    @check(admin_or_permissions(Permissions.MANAGE_MESSAGES))
    async def _filter_edit(self, ctx: InteractionContext, name: str) -> None:
        return await self._edit_filter(ctx, name, True)

    @filter_.subcommand(sub_cmd_name="view", sub_cmd_description="View a filter")
    @slash_option(
        name="name",
        description="Filter to view",
        autocomplete=True,
        opt_type=OptionType.STRING,
        required=True,
    )
    async def _filter_view(self, ctx: InteractionContext, name: str) -> None:
        f = await Filter.find_one(Filter.name == name, Filter.guild == ctx.guild.id)
        if not f:
            await ctx.send("That filter doesn't exist", ephemeral=True)
            return

        filters = "\n".join(f.filters)

        await ctx.send(f"Filter `{name}`:\n\n```\n{filters}\n```")

    @filter_.subcommand(sub_cmd_name="delete", sub_cmd_description="Delete a filter")
    @slash_option(
        name="name",
        description="Filter to delete",
        autocomplete=True,
        opt_type=OptionType.STRING,
        required=True,
    )
    @check(admin_or_permissions(Permissions.MANAGE_MESSAGES))
    async def _filter_delete(self, ctx: InteractionContext, name: str) -> None:
        f = await Filter.find_one(Filter.name == name, Filter.guild == ctx.guild.id)
        if not f:
            await ctx.send("That filter doesn't exist", ephemeral=True)
            return

        try:
            await f.delete()
        except Exception:
            self.bot.logger.debug(f"Failed to delete filter {name} in {ctx.guild.id}")

        await ctx.send(f"Filter `{name}` deleted")
        self.cache[ctx.guild.id].remove(f.name)

    @_filter_edit.autocomplete("name")
    @_filter_view.autocomplete("name")
    @_filter_delete.autocomplete("name")
    async def _autocomplete(self, ctx: AutocompleteContext) -> None:
        if not self.cache.get(ctx.guild.id):
            filters = await Filter.find(Filter.guild == ctx.guild.id).to_list()
            self.cache[ctx.guild.id] = [f.name for f in filters]
        results = process.extract(
            ctx.input_text, self.cache.get(ctx.guild.id), limit=25
        )
        choices = [{"name": r[0], "value": r[0]} for r in results]
        await ctx.send(choices=choices)


def setup(bot: Client) -> None:
    """Add FilterCog to JARVIS"""
    FilterCog(bot)
