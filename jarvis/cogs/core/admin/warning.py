"""JARVIS WarningCog."""
from datetime import datetime, timedelta, timezone

from interactions import InteractionContext, Permissions
from interactions.client.utils.misc_utils import get_all
from interactions.ext.paginators import Paginator
from interactions.models.discord.embed import EmbedField
from interactions.models.discord.user import Member
from interactions.models.internal.application_commands import (
    OptionType,
    slash_command,
    slash_option,
)
from interactions.models.internal.command import check
from jarvis_core.db.models import Warning

from jarvis.embeds.admin import warning_embed
from jarvis.utils import build_embed
from jarvis.utils.cogs import ModcaseCog
from jarvis.utils.permissions import admin_or_permissions


class WarningCog(ModcaseCog):
    """JARVIS WarningCog."""

    @slash_command(name="warn", description="Warn a user")
    @slash_option(
        name="user", description="User to warn", opt_type=OptionType.USER, required=True
    )
    @slash_option(
        name="reason",
        description="Reason for warning",
        opt_type=OptionType.STRING,
        required=True,
    )
    @slash_option(
        name="duration",
        description="Duration of warning in hours, default 24",
        opt_type=OptionType.INTEGER,
        required=False,
    )
    @check(admin_or_permissions(Permissions.MANAGE_GUILD))
    async def _warn(
        self, ctx: InteractionContext, user: Member, reason: str, duration: int = 24
    ) -> None:
        if len(reason) > 100:
            await ctx.send("Reason must be < 100 characters", ephemeral=True)
            return
        if duration <= 0:
            await ctx.send("Duration must be > 0", ephemeral=True)
            return
        elif duration >= 120:
            await ctx.send("Duration must be < 5 days", ephemeral=True)
            return
        if not await ctx.guild.fetch_member(user.id):
            await ctx.send("User not in guild", ephemeral=True)
            return
        await ctx.defer()
        expires_at = datetime.now(tz=timezone.utc) + timedelta(hours=duration)
        await Warning(
            user=user.id,
            reason=reason,
            admin=ctx.author.id,
            guild=ctx.guild.id,
            duration=duration,
            expires_at=expires_at,
            active=True,
        ).save()
        embed = warning_embed(user, reason, ctx.author)
        await ctx.send(embeds=embed)

    @slash_command(name="warnings", description="Get count of user warnings")
    @slash_option(
        name="user", description="User to view", opt_type=OptionType.USER, required=True
    )
    @slash_option(
        name="active",
        description="View active only",
        opt_type=OptionType.BOOLEAN,
        required=False,
    )
    # @check(admin_or_permissions(Permissions.MANAGE_GUILD))
    async def _warnings(
        self, ctx: InteractionContext, user: Member, active: bool = True
    ) -> None:
        warnings = (
            await Warning.find(
                Warning.user == user.id,
                Warning.guild == ctx.guild.id,
            )
            .sort(-Warning.created_at)
            .to_list()
        )
        if len(warnings) == 0:
            await ctx.defer(ephemeral=True)
            await ctx.send("That user has no warnings.", ephemeral=True)
            return
        active_warns = get_all(warnings, active=True)

        pages = []
        if active:
            if len(active_warns) == 0:
                embed = build_embed(
                    title="Warnings",
                    description=f"{len(warnings)} total | 0 currently active",
                    fields=[],
                )
                embed.set_author(name=user.username, icon_url=user.display_avatar.url)
                embed.set_thumbnail(url=ctx.guild.icon.url)
                pages.append(embed)
            else:
                fields = []
                for warn in active_warns:
                    admin = await ctx.guild.fetch_member(warn.admin)
                    ts = int(warn.created_at.timestamp())
                    admin_name = "||`[redacted]`||"
                    if admin:
                        admin_name = f"{admin.username}#{admin.discriminator}"
                    fields.append(
                        EmbedField(
                            name=f"<t:{ts}:F>",
                            value=f"{warn.reason}\nAdmin: {admin_name}\n\u200b",
                            inline=False,
                        )
                    )
                for i in range(0, len(fields), 5):
                    embed = build_embed(
                        title="Warnings",
                        description=(
                            f"{len(warnings)} total | {len(active_warns)} currently active"
                        ),
                        fields=fields[i : i + 5],
                    )
                    embed.set_author(
                        name=user.username + "#" + user.discriminator,
                        icon_url=user.display_avatar.url,
                    )
                    embed.set_thumbnail(url=ctx.guild.icon.url)
                    embed.set_footer(
                        text=f"{user.username}#{user.discriminator} | {user.id}"
                    )
                    pages.append(embed)
        else:
            fields = []
            for warn in warnings:
                ts = int(warn.created_at.timestamp())
                title = "[A] " if warn.active else "[I] "
                title += f"<t:{ts}:F>"
                fields.append(
                    EmbedField(
                        name=title,
                        value=warn.reason + "\n\u200b",
                        inline=False,
                    )
                )
            for i in range(0, len(fields), 5):
                embed = build_embed(
                    title="Warnings",
                    description=(
                        f"{len(warnings)} total | {len(active_warns)} currently active"
                    ),
                    fields=fields[i : i + 5],
                )
                embed.set_author(
                    name=user.username + "#" + user.discriminator,
                    icon_url=user.display_avatar.url,
                )
                embed.set_thumbnail(url=ctx.guild.icon.url)
                pages.append(embed)

        paginator = Paginator.create_from_embeds(self.bot, *pages, timeout=300)

        await paginator.send(ctx)
