"""JARVIS Admin Cogs."""
import logging

from interactions import Client

from jarvis.cogs.core.admin import (
    autoreact,
    ban,
    filters,
    kick,
    lock,
    lockdown,
    modcase,
    mute,
    purge,
    roleping,
    settings,
    temprole,
    verify,
    warning,
)


def setup(bot: Client) -> None:
    """Add admin cogs to JARVIS"""
    logger = logging.getLogger(__name__)
    msg = "Loaded jarvis.cogs.admin.{}"
    autoreact.AutoReactCog(bot)
    logger.debug(msg.format("autoreact"))
    ban.BanCog(bot)
    logger.debug(msg.format("ban"))
    filters.FilterCog(bot)
    logger.debug(msg.format("filters"))
    kick.KickCog(bot)
    logger.debug(msg.format("kick"))
    lock.LockCog(bot)
    logger.debug(msg.format("lock"))
    lockdown.LockdownCog(bot)
    logger.debug(msg.format("lockdown"))
    modcase.CaseCog(bot)
    logger.debug(msg.format("modcase"))
    mute.MuteCog(bot)
    logger.debug(msg.format("mute"))
    purge.PurgeCog(bot)
    logger.debug(msg.format("purge"))
    roleping.RolepingCog(bot)
    logger.debug(msg.format("roleping"))
    settings.SettingsCog(bot)
    logger.debug(msg.format("settings"))
    temprole.TemproleCog(bot)
    logger.debug(msg.format("temprole"))
    verify.VerifyCog(bot)
    logger.debug(msg.format("verify"))
    warning.WarningCog(bot)
    logger.debug(msg.format("warning"))
