"""JARVIS Verify Cog."""
import asyncio
import logging
from random import randint

from interactions import Client, Extension, InteractionContext
from interactions.models.discord.components import Button, ButtonStyle, spread_to_rows
from interactions.models.internal.application_commands import slash_command
from interactions.models.internal.command import cooldown
from interactions.models.internal.cooldowns import Buckets
from jarvis_core.db.models import Setting


def create_layout() -> list:
    """Create verify component layout."""
    buttons = []
    yes = randint(0, 2)  # noqa: S311
    for i in range(3):
        label = "YES" if i == yes else "NO"
        id = f"no_{i}" if not i == yes else "yes"
        color = ButtonStyle.GREEN if i == yes else ButtonStyle.RED
        buttons.append(
            Button(
                style=color,
                label=label,
                custom_id=f"verify_button||{id}",
            )
        )
    return spread_to_rows(*buttons, max_in_row=3)


class VerifyCog(Extension):
    """JARVIS Verify Cog."""

    def __init__(self, bot: Client):
        self.bot = bot
        self.logger = logging.getLogger(__name__)

    @slash_command(name="verify", description="Verify that you've read the rules")
    @cooldown(bucket=Buckets.USER, rate=1, interval=30)
    async def _verify(self, ctx: InteractionContext) -> None:
        role = await Setting.find_one(Setting.guild == ctx.guild.id, Setting.setting == "verified")
        if not role:
            message = await ctx.send("This guild has not enabled verification", ephemeral=True)
            return
        verified_role = await ctx.guild.fetch_role(role.value)
        if not verified_role:
            await ctx.send("This guild has not enabled verification", ephemeral=True)
            await role.delete()
            return

        if verified_role in ctx.author.roles:
            await ctx.send("You are already verified.", ephemeral=True)
            return
        components = create_layout()
        message = await ctx.send(
            content=f"{ctx.author.mention}, please press the button that says `YES`.",
            components=components,
        )

        try:
            verified = False
            while not verified:
                response = await self.bot.wait_for_component(
                    messages=message,
                    check=lambda x: ctx.author.id == x.ctx.author.id,
                    timeout=30,
                )

                correct = response.ctx.custom_id.split("||")[-1] == "yes"
                if correct:
                    for row in components:
                        for component in row.components:
                            component.disabled = True
                    setting = await Setting.find_one(Setting.guild == ctx.guild.id, Setting.setting == "verified")
                    try:
                        role = await ctx.guild.fetch_role(setting.value)
                        await ctx.author.add_role(role, reason="Verification passed")
                    except AttributeError:
                        self.logger.warning("Verified role deleted before verification finished")
                    setting = await Setting.find_one(Setting.guild == ctx.guild.id, Setting.setting == "unverified")
                    if setting:
                        try:
                            role = await ctx.guild.fetch_role(setting.value)
                            await ctx.author.remove_role(role, reason="Verification passed")
                        except AttributeError:
                            self.logger.warning("Unverified role deleted before verification finished")

                    await response.ctx.edit_origin(
                        content=f"Welcome, {ctx.author.mention}. Please enjoy your stay.",
                        components=components,
                    )
                    await response.ctx.message.delete(delay=5)
                    self.logger.debug(f"User {ctx.author.id} verified successfully")
                else:
                    await response.ctx.edit_origin(
                        content=(f"{ctx.author.mention}, incorrect. " "Please press the button that says `YES`")
                    )
        except asyncio.TimeoutError:
            await message.delete(delay=2)
            self.logger.debug(f"User {ctx.author.id} failed to verify before timeout")


def setup(bot: Client) -> None:
    """Add VerifyCog to JARVIS"""
    VerifyCog(bot)
