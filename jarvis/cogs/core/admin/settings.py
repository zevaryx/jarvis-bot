"""JARVIS Settings Management Cog."""
import asyncio
import logging
from typing import Any

from interactions import AutocompleteContext, Client, Extension, InteractionContext
from interactions.models.discord.channel import GuildText
from interactions.models.discord.components import ActionRow, Button, ButtonStyle
from interactions.models.discord.embed import EmbedField
from interactions.models.discord.enums import Permissions
from interactions.models.discord.role import Role
from interactions.models.internal.application_commands import (
    OptionType,
    SlashCommand,
    slash_option,
)
from interactions.models.internal.command import check
from jarvis_core.db.models import Setting
from thefuzz import process

from jarvis.utils import build_embed
from jarvis.utils.permissions import admin_or_permissions


class SettingsCog(Extension):
    """JARVIS Settings Management Cog."""

    def __init__(self, bot: Client):
        self.bot = bot
        self.logger = logging.getLogger(__name__)

    async def update_settings(self, setting: str, value: Any, guild: int) -> bool:
        """Update a guild setting."""
        existing = await Setting.find_one(Setting.setting == setting, Setting.guild == guild)
        if not existing:
            existing = Setting(setting=setting, guild=guild, value=value)
        existing.value = value
        updated = await existing.save()

        return updated is not None

    async def delete_settings(self, setting: str, guild: int) -> bool:
        """Delete a guild setting."""
        existing = await Setting.find_one(Setting.setting == setting, Setting.guild == guild)
        if existing:
            return await existing.delete()
        return False

    settings = SlashCommand(name="settings", description="Control guild settings")
    set_ = settings.group(name="set", description="Set a setting")
    unset = settings.group(name="unset", description="Unset a setting")

    @set_.subcommand(
        sub_cmd_name="modlog",
        sub_cmd_description="Set Moglod channel",
    )
    @slash_option(name="channel", description="ModLog Channel", opt_type=OptionType.CHANNEL, required=True)
    @check(admin_or_permissions(Permissions.MANAGE_GUILD))
    async def _set_modlog(self, ctx: InteractionContext, channel: GuildText) -> None:
        if not isinstance(channel, GuildText):
            await ctx.send("Channel must be a GuildText", ephemeral=True)
            return
        await self.update_settings("modlog", channel.id, ctx.guild.id)
        await ctx.send(f"Settings applied. New modlog channel is {channel.mention}")

    @set_.subcommand(
        sub_cmd_name="activitylog",
        sub_cmd_description="Set Activitylog channel",
    )
    @slash_option(
        name="channel",
        description="Activitylog Channel",
        opt_type=OptionType.CHANNEL,
        required=True,
    )
    @check(admin_or_permissions(Permissions.MANAGE_GUILD))
    async def _set_activitylog(self, ctx: InteractionContext, channel: GuildText) -> None:
        if not isinstance(channel, GuildText):
            await ctx.send("Channel must be a GuildText", ephemeral=True)
            return
        await self.update_settings("activitylog", channel.id, ctx.guild.id)
        await ctx.send(f"Settings applied. New activitylog channel is {channel.mention}")

    @set_.subcommand(sub_cmd_name="massmention", sub_cmd_description="Set massmention output")
    @slash_option(
        name="amount",
        description="Amount of mentions (0 to disable)",
        opt_type=OptionType.INTEGER,
        required=True,
    )
    @check(admin_or_permissions(Permissions.MANAGE_GUILD))
    async def _set_massmention(self, ctx: InteractionContext, amount: int) -> None:
        await ctx.defer()
        await self.update_settings("massmention", amount, ctx.guild.id)
        await ctx.send(f"Settings applied. New massmention limit is {amount}")

    @set_.subcommand(sub_cmd_name="verified", sub_cmd_description="Set verified role")
    @slash_option(name="role", description="Verified role", opt_type=OptionType.ROLE, required=True)
    @check(admin_or_permissions(Permissions.MANAGE_GUILD))
    async def _set_verified(self, ctx: InteractionContext, role: Role) -> None:
        if role.id == ctx.guild.id:
            await ctx.send("Cannot set verified to `@everyone`", ephemeral=True)
            return
        if role.bot_managed or not role.is_assignable:
            await ctx.send(
                "Cannot assign this role, try lowering it below my role or using a different role",
                ephemeral=True,
            )
            return
        await ctx.defer()
        await self.update_settings("verified", role.id, ctx.guild.id)
        await ctx.send(f"Settings applied. New verified role is `{role.name}`")

    @set_.subcommand(sub_cmd_name="unverified", sub_cmd_description="Set unverified role")
    @slash_option(name="role", description="Unverified role", opt_type=OptionType.ROLE, required=True)
    @check(admin_or_permissions(Permissions.MANAGE_GUILD))
    async def _set_unverified(self, ctx: InteractionContext, role: Role) -> None:
        if role.id == ctx.guild.id:
            await ctx.send("Cannot set unverified to `@everyone`", ephemeral=True)
            return
        if role.bot_managed or not role.is_assignable:
            await ctx.send(
                "Cannot assign this role, try lowering it below my role or using a different role",
                ephemeral=True,
            )
            return
        await ctx.defer()
        await self.update_settings("unverified", role.id, ctx.guild.id)
        await ctx.send(f"Settings applied. New unverified role is `{role.name}`")

    @set_.subcommand(sub_cmd_name="noinvite", sub_cmd_description="Set if invite deletion should happen")
    @slash_option(name="active", description="Active?", opt_type=OptionType.BOOLEAN, required=True)
    @check(admin_or_permissions(Permissions.MANAGE_GUILD))
    async def _set_invitedel(self, ctx: InteractionContext, active: bool) -> None:
        await ctx.defer()
        await self.update_settings("noinvite", active, ctx.guild.id)
        await ctx.send(f"Settings applied. Automatic invite active: {active}")

    @set_.subcommand(sub_cmd_name="notify", sub_cmd_description="Notify users of admin action?")
    @slash_option(name="active", description="Notify?", opt_type=OptionType.BOOLEAN, required=True)
    @check(admin_or_permissions(Permissions.MANAGE_GUILD))
    async def _set_notify(self, ctx: InteractionContext, active: bool) -> None:
        await ctx.defer()
        await self.update_settings("notify", active, ctx.guild.id)
        await ctx.send(f"Settings applied. Notifications active: {active}")

    @set_.subcommand(sub_cmd_name="log_ignore", sub_cmd_description="Ignore a channel for ActivityLog")
    @slash_option(name="channel", description="Channel to ignore", opt_type=OptionType.CHANNEL, required=True)
    @check(admin_or_permissions(Permissions.MANAGE_GUILD))
    async def _add_log_ignore(self, ctx: InteractionContext, channel: GuildText) -> None:
        if not isinstance(channel, GuildText):
            await ctx.send("Channel must be a GuildText", ephemeral=True)
            return
        setting: Setting = await Setting.find_one(Setting.guild == ctx.guild.id, Setting.setting == "log_ignore")
        if not setting:
            setting = Setting(guild=ctx.guild.id, setting="log_ignore", value=[])
        if not setting.value:
            setting.value = []
        if channel in setting.value:
            await ctx.send("Channel already ignored", ephemeral=True)
            return
        setting.value.append(channel.id)
        await setting.save()
        await ctx.send("Channel added to ActivityLog ignore list")

    # Unset
    @unset.subcommand(
        sub_cmd_name="modlog",
        sub_cmd_description="Unset Modlog channel",
    )
    @check(admin_or_permissions(Permissions.MANAGE_GUILD))
    async def _unset_modlog(self, ctx: InteractionContext) -> None:
        await ctx.defer()
        await self.delete_settings("modlog", ctx.guild.id)
        await ctx.send("Setting `modlog` unset")

    @unset.subcommand(
        sub_cmd_name="activitylog",
        sub_cmd_description="Unset Activitylog channel",
    )
    @check(admin_or_permissions(Permissions.MANAGE_GUILD))
    async def _unset_activitylog(self, ctx: InteractionContext) -> None:
        await ctx.defer()
        await self.delete_settings("activitylog", ctx.guild.id)
        await ctx.send("Setting `activitylog` unset")

    @unset.subcommand(sub_cmd_name="massmention", sub_cmd_description="Unset massmention output")
    @check(admin_or_permissions(Permissions.MANAGE_GUILD))
    async def _unset_massmention(self, ctx: InteractionContext) -> None:
        await ctx.defer()
        await self.delete_settings("massmention", ctx.guild.id)
        await ctx.send("Setting `massmention` unset")

    @unset.subcommand(sub_cmd_name="verified", sub_cmd_description="Unset verified role")
    @check(admin_or_permissions(Permissions.MANAGE_GUILD))
    async def _unset_verified(self, ctx: InteractionContext) -> None:
        await ctx.defer()
        await self.delete_settings("verified", ctx.guild.id)
        await ctx.send("Setting `verified` unset")

    @unset.subcommand(sub_cmd_name="unverified", sub_cmd_description="Unset unverified role")
    @check(admin_or_permissions(Permissions.MANAGE_GUILD))
    async def _unset_unverified(self, ctx: InteractionContext) -> None:
        await ctx.defer()
        await self.delete_settings("unverified", ctx.guild.id)
        await ctx.send("Setting `unverified` unset")

    @unset.subcommand(sub_cmd_name="noinvite", sub_cmd_description="Unset if invite deletion should happen")
    @check(admin_or_permissions(Permissions.MANAGE_GUILD))
    async def _unset_invitedel(self, ctx: InteractionContext, active: bool) -> None:
        await ctx.defer()
        await self.delete_settings("noinvite", ctx.guild.id)
        await ctx.send(f"Setting `{active}` unset")

    @unset.subcommand(sub_cmd_name="notify", sub_cmd_description="Unset admin action notifications")
    @check(admin_or_permissions(Permissions.MANAGE_GUILD))
    async def _unset_notify(self, ctx: InteractionContext) -> None:
        await ctx.defer()
        await self.delete_settings("notify", ctx.guild.id)
        await ctx.send("Setting `notify` unset")

    @unset.subcommand(sub_cmd_name="log_ignore", sub_cmd_description="Add a channel for ActivityLog")
    @slash_option(
        name="channel",
        description="Channel to stop ignoring",
        opt_type=OptionType.STRING,
        required=True,
        autocomplete=True,
    )
    @check(admin_or_permissions(Permissions.MANAGE_GUILD))
    async def _remove_log_ignore(self, ctx: InteractionContext, channel: str) -> None:
        channel = int(channel)
        setting = await Setting.find_one(Setting.guild == ctx.guild.id, Setting.setting == "log_ignore")
        if not setting or channel not in setting.value:
            await ctx.send("Channel not being ignored", ephemeral=True)
            return
        setting.value.remove(channel)
        await setting.save()
        await ctx.send("Channel no longer being ignored")

    @_remove_log_ignore.autocomplete(option_name="channel")
    async def _channel_search(self, ctx: AutocompleteContext, channel: str) -> None:
        setting = await Setting.find_one(Setting.guild == ctx.guild.id, Setting.setting == "log_ignore")
        if not setting:
            return {}
        channels = [ctx.guild.get_channel(x) for x in setting.value]
        channels = {c.name: c.id for c in channels if c}
        options = process.extract(channel, list(channels.keys()), limit=25)
        await ctx.send(choices=[{"name": c[0], "value": str(channels[c[0]])} for c in options])

    @settings.subcommand(sub_cmd_name="view", sub_cmd_description="View settings")
    @check(admin_or_permissions(Permissions.MANAGE_GUILD))
    async def _view(self, ctx: InteractionContext) -> None:
        settings = Setting.find(Setting.guild == ctx.guild.id)

        fields = []
        async for setting in settings:
            value = setting.value
            if setting.setting in ["unverified", "verified", "mute"]:
                try:
                    value = await ctx.guild.fetch_role(value)
                except KeyError:
                    await setting.delete()
                    continue
                if value:
                    value = value.mention
                else:
                    await setting.delete()
                    continue
            elif setting.setting in ["activitylog", "modlog"]:
                value = await ctx.guild.fetch_channel(value)
                if value:
                    value = value.mention
                else:
                    await setting.delete()
                    continue
            elif setting.setting == "log_ignore":
                names = []
                for v in setting.value:
                    channel = ctx.guild.get_channel(v)
                    if channel:
                        names.append(channel.mention)
                value = ", ".join(names) if names else "None"
            fields.append(EmbedField(name=setting.setting, value=str(value) or "N/A", inline=False))

        embed = build_embed(title="Current Settings", description="", fields=fields)

        await ctx.send(embeds=embed)

    @settings.subcommand(sub_cmd_name="clear", sub_cmd_description="Clear all settings")
    @check(admin_or_permissions(Permissions.MANAGE_GUILD))
    async def _clear(self, ctx: InteractionContext) -> None:
        components = [
            ActionRow(
                Button(style=ButtonStyle.RED, emoji="✖️", custom_id=f"{ctx.guild.id}|set_clear|no"),
                Button(style=ButtonStyle.GREEN, emoji="✔️", custom_id=f"{ctx.guild.id}|set_clear|yes"),
            )
        ]
        message = await ctx.send("***Are you sure?***", components=components)
        try:
            context = await self.bot.wait_for_component(
                check=lambda x: ctx.author.id == x.ctx.author.id,
                messages=message,
                timeout=60 * 5,
            )
            if context.ctx.custom_id == f"{ctx.guild.id}|set_clear|yes":
                async for setting in Setting.find(Setting.guild == ctx.guild.id):
                    await setting.delete()
                content = "Guild settings cleared"
            else:
                content = "Guild settings not cleared"
            for row in components:
                for component in row.components:
                    component.disabled = True
            await context.ctx.edit_origin(content=content, components=components)
        except asyncio.TimeoutError:
            for row in components:
                for component in row.components:
                    component.disabled = True
            await message.edit(content="Guild settings not cleared", components=components)


def setup(bot: Client) -> None:
    """Add SettingsCog to JARVIS"""
    SettingsCog(bot)
