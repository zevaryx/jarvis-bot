"""JARVIS Social Cogs."""

from interactions import Client


def setup(bot: Client) -> None:
    """Add social cogs to JARVIS"""
    # Unfortunately there's no social cogs anymore
    # Mastodon will come in the future
