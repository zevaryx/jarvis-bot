"""JARVIS Remind Me Cog."""
import asyncio
import logging
import re
from datetime import datetime, timezone
from typing import List

import pytz
from croniter import croniter
from dateparser import parse
from dateparser_data.settings import default_parsers
from interactions import AutocompleteContext, Client, Extension, InteractionContext
from interactions.models.discord.channel import GuildChannel
from interactions.models.discord.components import ActionRow, Button
from interactions.models.discord.embed import Embed, EmbedField
from interactions.models.discord.enums import ButtonStyle
from interactions.models.discord.modal import InputText, Modal, TextStyles
from interactions.models.internal.application_commands import (
    OptionType,
    SlashCommand,
    slash_option,
)
from jarvis_core.db.models import Reminder
from thefuzz import process

from jarvis.utils import build_embed

valid = re.compile(r"[\w\s\-\\/.!@?#$%^*()+=<>:'\",\u0080-\U000E0FFF]*")
time_pattern = re.compile(r"(\d+\.?\d?[s|m|h|d|w]{1})\s?", flags=re.IGNORECASE)
invites = re.compile(
    r"(?:https?://)?(?:www.)?(?:discord.(?:gg|io|me|li)|discord(?:app)?.com/invite)/([^\s/]+?)(?=\b)",  # noqa: E501
    flags=re.IGNORECASE,
)


class RemindmeCog(Extension):
    """JARVIS Remind Me Cog."""

    def __init__(self, bot: Client):
        self.bot = bot
        self.logger = logging.getLogger(__name__)

    reminders = SlashCommand(name="reminders", description="Manage reminders")

    @reminders.subcommand(sub_cmd_name="set", sub_cmd_description="Set a reminder")
    @slash_option(
        name="timezone",
        description="Timezone to use",
        opt_type=OptionType.STRING,
        required=False,
        autocomplete=True,
    )
    @slash_option(
        name="private",
        description="Send as DM?",
        opt_type=OptionType.BOOLEAN,
        required=False,
    )
    async def _remindme(
        self,
        ctx: InteractionContext,
        timezone: str = "UTC",
        private: bool = None,
    ) -> None:
        if private is None and ctx.guild:
            private = ctx.guild.member_count >= 5000
        elif private is None and not ctx.guild:
            private = False
        timezone = pytz.timezone(timezone)
        modal = Modal(
            *[
                InputText(
                    label="What to remind you?",
                    placeholder="Reminder",
                    style=TextStyles.PARAGRAPH,
                    custom_id="message",
                    max_length=500,
                ),
                InputText(
                    label="When to remind you?",
                    placeholder="1h 30m | in 5 minutes | November 11, 4011",
                    style=TextStyles.SHORT,
                    custom_id="delay",
                ),
                InputText(
                    label="Cron pattern for repeating",
                    placeholder="0 12 * * *",
                    style=TextStyles.SHORT,
                    max_length=40,
                    custom_id="cron",
                    required=False,
                ),
            ],
            title="Set your reminder!",
        )

        await ctx.send_modal(modal)
        try:
            response = await self.bot.wait_for_modal(
                modal, author=ctx.author.id, timeout=60 * 5
            )
            message = response.responses.get("message").strip()
            delay = response.responses.get("delay").strip()
            cron = response.responses.get("cron").strip()
        except asyncio.TimeoutError:
            return
        if len(message) > 500:
            await response.send("Reminder cannot be > 500 characters.", ephemeral=True)
            return
        elif invites.search(message):
            await response.send(
                "Listen, don't use this to try and bypass the rules",
                ephemeral=True,
            )
            return
        # elif not valid.fullmatch(message):
        #     await response.send(
        #         "Hey, you should probably make this readable", ephemeral=True
        #     )
        #     return
        elif len(message) == 0:
            await response.send(
                "Hey, you should probably add content to your reminder", ephemeral=True
            )
            return
        elif cron and not croniter.is_valid(cron):
            await response.send(
                f"Invalid cron: {cron}\n\nUse https://crontab.guru to help",
                ephemeral=True,
            )
            return

        base_settings = {
            "PREFER_DATES_FROM": "future",
            "TIMEZONE": str(timezone),
            "RETURN_AS_TIMEZONE_AWARE": True,
        }
        rt_settings = base_settings.copy()
        rt_settings["PARSERS"] = [
            x for x in default_parsers if x not in ["absolute-time", "timestamp"]
        ]

        rt_remind_at = parse(delay, settings=rt_settings)

        at_settings = base_settings.copy()
        at_settings["PARSERS"] = [x for x in default_parsers if x != "relative-time"]
        at_remind_at = parse(delay, settings=at_settings)

        if rt_remind_at:
            remind_at = rt_remind_at
        elif at_remind_at:
            remind_at = at_remind_at
        else:
            self.logger.debug(f"Failed to parse delay: {delay}")
            await response.send(
                f"`{delay}` is not a parsable date, please try again",
                ephemeral=True,
            )
            return

        if remind_at < datetime.now(tz=timezone):
            await response.send(
                f"`{delay}` is in the past. Past reminders aren't allowed",
                ephemeral=True,
            )
            return

        elif remind_at < datetime.now(tz=timezone):
            pass

        r = Reminder(
            user=ctx.author.id,
            channel=ctx.channel.id,
            guild=ctx.guild.id if ctx.guild else ctx.author.id,
            message=message,
            remind_at=remind_at,
            private=private,
            repeat=cron,
            timezone=str(timezone),
            active=True,
        )

        await r.save()

        fields = [
            EmbedField(name="Message", value=message),
            EmbedField(
                name="When",
                value=f"<t:{int(remind_at.timestamp())}:F> (<t:{int(remind_at.timestamp())}:R>)",
                inline=False,
            ),
        ]

        if r.repeat:
            c = croniter(cron, remind_at)
            fields.append(EmbedField(name="Repeat Schedule", value=f"`{cron}`"))
            next_5 = [c.get_next() for _ in range(5)]

            next_5_str = "\n".join(f"<t:{int(x)}:F> (<t:{int(x)}:R>)" for x in next_5)
            fields.append(EmbedField(name="Next 5 runs", value=next_5_str))

        embed = build_embed(
            title="Reminder Set",
            description=f"{ctx.author.mention} set a reminder",
            fields=fields,
        )

        embed.set_author(
            name=ctx.author.username,
            icon_url=ctx.author.display_avatar.url,
        )
        embed.set_thumbnail(url=ctx.author.display_avatar.url)
        delete_button = Button(
            style=ButtonStyle.DANGER,
            emoji="🗑️",
            custom_id=f"delete|{ctx.author.id}",
        )
        components = [delete_button]
        if not r.guild == ctx.author.id:
            copy_button = Button(
                style=ButtonStyle.GREEN, emoji="📋", custom_id=f"copy|rme|{r.id}"
            )
            components.append(copy_button)
        private = private if private is not None else False
        components = [ActionRow(*components)]
        await response.send(embeds=embed, components=components, ephemeral=private)

    async def get_reminders_embed(
        self, ctx: InteractionContext, reminders: List[Reminder]
    ) -> Embed:
        """Build embed for paginator."""
        fields = []
        for reminder in reminders:
            if reminder.private and isinstance(ctx.channel, GuildChannel):
                fields.append(
                    EmbedField(
                        name=f"<t:{int(reminder.remind_at.timestamp())}:F> (<t:{int(reminder.remind_at.timestamp())}:R>)",
                        value="Please DM me this command to view the content of this reminder",
                        inline=False,
                    )
                )
            else:
                fields.append(
                    EmbedField(
                        name=f"<t:{int(reminder.remind_at.timestamp())}:F> (<t:{int(reminder.remind_at.timestamp())}:R>)",
                        value=f"{reminder.message}\n\u200b",
                        inline=False,
                    )
                )

        embed = build_embed(
            title=f"{len(reminders)} Active Reminder(s)",
            description=f"All active reminders for {ctx.author.mention}",
            fields=fields,
        )

        embed.set_author(
            name=ctx.author.username,
            icon_url=ctx.author.display_avatar.url,
        )
        embed.set_thumbnail(url=ctx.author.display_avatar.url)

        return embed

    @reminders.subcommand(sub_cmd_name="list", sub_cmd_description="List reminders")
    async def _list(self, ctx: InteractionContext) -> None:
        reminders = await Reminder.find(
            Reminder.user == ctx.author.id, Reminder.active == True
        ).to_list()
        if not reminders:
            await ctx.send("You have no reminders set.", ephemeral=True)
            return

        embed = await self.get_reminders_embed(ctx, reminders)
        components = Button(
            style=ButtonStyle.DANGER, emoji="🗑️", custom_id=f"delete|{ctx.author.id}"
        )
        await ctx.send(embeds=embed, components=components)

    @reminders.subcommand(
        sub_cmd_name="delete", sub_cmd_description="Delete a reminder"
    )
    @slash_option(
        name="content",
        description="Content of the reminder",
        opt_type=OptionType.STRING,
        required=True,
        autocomplete=True,
    )
    async def _delete(self, ctx: InteractionContext, content: str) -> None:
        reminder = await Reminder.get(content)
        if not reminder:
            await ctx.send(f"Reminder `{content}` does not exist", ephemeral=True)
            return

        ts = int(reminder.remind_at.timestamp())

        fields = [EmbedField(name=f"<t:{ts}:F>", value=reminder.message, inline=False)]

        embed = build_embed(
            title="Deleted Reminder(s)",
            description="",
            fields=fields,
        )

        embed.set_author(
            name=ctx.author.display_name,
            icon_url=ctx.author.display_avatar.url,
        )
        embed.set_thumbnail(url=ctx.author.display_avatar.url)

        components = Button(
            style=ButtonStyle.DANGER, emoji="🗑️", custom_id=f"delete|{ctx.author.id}"
        )
        try:
            await reminder.delete()
        except Exception:
            self.logger.debug("Ignoring deletion error")
        await ctx.send(embeds=embed, ephemeral=reminder.private, components=components)

    @reminders.subcommand(
        sub_cmd_name="fetch",
        sub_cmd_description="Fetch a reminder that failed to send",
    )
    @slash_option(
        name="content",
        description="Content of the reminder",
        opt_type=OptionType.STRING,
        required=True,
        autocomplete=True,
    )
    async def _fetch(self, ctx: InteractionContext, content: str) -> None:
        reminder = await Reminder.find_one(Reminder.id == content)
        if not reminder:
            await ctx.send(f"Reminder `{content}` does not exist", ephemeral=True)
            return

        ts = int(reminder.remind_at.timestamp())
        cts = int(reminder.created_at.timestamp())

        fields = [
            EmbedField(name="Remind At", value=f"<t:{ts}:F> (<t:{ts}:R>)"),
            EmbedField(name="Created At", value=f"<t:{cts}:F> (<t:{cts}:R>)"),
        ]

        embed = build_embed(
            title="You have a reminder!", description=reminder.message, fields=fields
        )
        embed.set_author(
            name=ctx.author.display_name,
            icon_url=ctx.author.display_avatar.url,
        )

        embed.set_thumbnail(url=ctx.author.display_avatar.url)
        components = Button(
            style=ButtonStyle.DANGER, emoji="🗑️", custom_id=f"delete|{ctx.author.id}"
        )
        await ctx.send(embeds=embed, ephemeral=reminder.private, components=components)
        if reminder.remind_at <= datetime.now(tz=timezone.utc) and not reminder.active:
            try:
                await reminder.delete()
            except Exception:
                self.logger.debug("Ignoring deletion error")

    @_fetch.autocomplete("content")
    @_delete.autocomplete("content")
    async def _search_reminders(self, ctx: AutocompleteContext) -> None:
        reminders = await Reminder.find(Reminder.user == ctx.author.id).to_list()
        lookup = {
            f"[{r.created_at.strftime('%d/%m/%Y %H:%M.%S')}] {r.message}": str(r.id)
            for r in reminders
        }
        results = process.extract(ctx.input_text, list(lookup.keys()), limit=5)
        choices = [{"name": r[0], "value": lookup[r[0]]} for r in results]
        await ctx.send(choices=choices)

    @_remindme.autocomplete("timezone")
    async def _timezone_autocomplete(self, ctx: AutocompleteContext):
        results = process.extract(ctx.input_text, pytz.all_timezones_set, limit=5)
        choices = [{"name": r[0], "value": r[0]} for r in results if r[1] > 80.0]
        await ctx.send(choices)


def setup(bot: Client) -> None:
    """Add RemindmeCog to JARVIS"""
    RemindmeCog(bot)
