"""JARVIS Core Cogs."""
from interactions import Client

from jarvis.cogs.core import admin, botutil, remindme, socials, util


def setup(bot: Client) -> None:
    """Add core cogs to JARVIS"""
    admin.setup(bot)
    botutil.setup(bot)
    remindme.setup(bot)
    socials.setup(bot)
    util.setup(bot)
