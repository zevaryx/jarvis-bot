"""JARVIS Role Giver Cog."""
import asyncio
import logging

from interactions import (
    AutocompleteContext,
    Client,
    Extension,
    InteractionContext,
    Permissions,
    listen,
)
from interactions.client.utils.misc_utils import get
from interactions.models.discord.components import (
    ActionRow,
    Button,
    StringSelectMenu,
    StringSelectOption,
)
from interactions.models.discord.embed import EmbedField
from interactions.models.discord.enums import ButtonStyle
from interactions.models.discord.role import Role
from interactions.models.internal.application_commands import (
    OptionType,
    SlashCommand,
    slash_option,
)
from interactions.models.internal.command import check, cooldown
from interactions.models.internal.cooldowns import Buckets
from jarvis_core.db.models import Rolegiver
from thefuzz import process

from jarvis.utils import build_embed
from jarvis.utils.permissions import admin_or_permissions


class RolegiverCog(Extension):
    """JARVIS Role Giver Cog."""

    def __init__(self, bot: Client):
        self.bot = bot
        self.logger = logging.getLogger(__name__)
        self._group_cache: dict[int, dict[str, dict[str, int]]] = {}
        """`{GUILD_ID: {GROUP_NAME: {ROLE_NAME: ROLE_ID}}}`"""

    @listen()
    async def on_ready(self) -> None:
        """NAFF on_ready hook for loading cache."""
        async for rolegiver in Rolegiver.find():
            if not rolegiver.group:
                rolegiver.group = "Default"
            guild = await self.bot.fetch_guild(rolegiver.guild)
            if not guild:
                await rolegiver.delete()
                continue
            if guild.id not in self._group_cache:
                self._group_cache[guild.id] = {rolegiver.group: {}}
            if rolegiver.group not in self._group_cache[guild.id]:
                self._group_cache[guild.id][rolegiver.group] = {}
            roles = []
            for role_id in rolegiver.roles:
                role = await guild.fetch_role(role_id)
                if role:
                    roles.append(role.id)
                else:
                    continue
                self._group_cache[guild.id][rolegiver.group][role.name] = role.id
            rolegiver.roles = roles
            await rolegiver.save()

    rolegiver = SlashCommand(
        name="rolegiver", description="Allow users to choose their own roles"
    )

    rg_group = rolegiver.group(name="group", description="Manage rolegiver groups")

    @rg_group.subcommand(
        sub_cmd_name="create", sub_cmd_description="Create a rolegiver group"
    )
    @slash_option(
        name="group",
        description="Group to create",
        opt_type=OptionType.STRING,
        required=True,
    )
    @check(admin_or_permissions(Permissions.MANAGE_GUILD))
    async def _group_create(self, ctx: InteractionContext, group: str):
        if await Rolegiver.find_one(
            Rolegiver.guild == ctx.guild.id, Rolegiver.group == group
        ):
            await ctx.send("Group already exists!", ephemeral=True)
            return

        rolegiver = Rolegiver(guild=ctx.guild.id, group=group, roles=[])
        await rolegiver.save()

        await ctx.send(f"Rolegiver group {group} created!")

    @rg_group.subcommand(
        sub_cmd_name="delete", sub_cmd_description="DDelete a rolegiver group"
    )
    @slash_option(
        name="group",
        description="Group to delete",
        opt_type=OptionType.STRING,
        required=True,
        autocomplete=True,
    )
    @check(admin_or_permissions(Permissions.MANAGE_GUILD))
    async def _group_delete(self, ctx: InteractionContext, group: str):
        if rolegiver := await Rolegiver.find_one(
            Rolegiver.guild == ctx.guild.id, Rolegiver.group == group
        ):
            await rolegiver.delete()
            await ctx.send(f"Rolegiver group {group} deleted!")
        else:
            await ctx.send(f"Rolegiver group {group} does not exist!", ephemeral=True)

    @rolegiver.subcommand(
        sub_cmd_name="add",
        sub_cmd_description="Add a role to rolegiver",
    )
    @slash_option(
        name="role", description="Role to add", opt_type=OptionType.ROLE, required=True
    )
    @slash_option(
        name="group",
        description="Group to add to",
        opt_type=OptionType.STRING,
        required=False,
        autocomplete=True,
    )
    @check(admin_or_permissions(Permissions.MANAGE_GUILD))
    async def _rolegiver_add(
        self, ctx: InteractionContext, role: Role, group: str = "Default"
    ) -> None:
        if role.id == ctx.guild.id:
            await ctx.send("Cannot add `@everyone` to rolegiver", ephemeral=True)
            return

        if role.bot_managed or not role.is_assignable:
            await ctx.send(
                "Cannot assign this role, try lowering it below my role or using a different role",
                ephemeral=True,
            )
            return

        rolegiver = await Rolegiver.find_one(
            Rolegiver.guild == ctx.guild.id, Rolegiver.group == group
        )
        if rolegiver and rolegiver.roles and role.id in rolegiver.roles:
            await ctx.send("Role already in rolegiver", ephemeral=True)
            return

        elif rolegiver and len(rolegiver.roles) >= 15:
            await ctx.send(
                f"Maximum roles in group {group}. Please make a new group",
                ephemeral=True,
            )
            return

        if not rolegiver:
            rolegiver = Rolegiver(guild=ctx.guild.id, roles=[], group=group)

        rolegiver.roles = rolegiver.roles or []

        rolegiver.roles.append(role.id)
        await rolegiver.save()

        roles = []
        for role_id in rolegiver.roles:
            if role_id == role.id:
                continue
            e_role = await ctx.guild.fetch_role(role_id)
            if not e_role:
                continue
            roles.append(e_role)
        if roles:
            roles.sort(key=lambda x: -x.position)

        value = "\n".join([r.mention for r in roles]) if roles else "None"
        fields = [
            EmbedField(name="New Role", value=f"{role.mention}"),
            EmbedField(name="Existing Role(s)", value=value),
        ]

        embed = build_embed(
            title="Rolegiver Updated",
            description=f"New role added to rolegiver group {group}",
            fields=fields,
        )

        embed.set_thumbnail(url=ctx.guild.icon.url)

        embed.set_footer(text=f"{ctx.author.username} | {ctx.author.id}")
        components = Button(
            style=ButtonStyle.DANGER, emoji="🗑️", custom_id=f"delete|{ctx.author.id}"
        )
        await ctx.send(embeds=embed, components=components)

        if ctx.guild.id not in self._group_cache:
            self._group_cache[ctx.guild.id] = {group: {}}
        self._group_cache[ctx.guild.id][group].update({role.name: role.id})

    @rolegiver.subcommand(
        sub_cmd_name="remove", sub_cmd_description="Remove a role from rolegiver"
    )
    @slash_option(
        name="group",
        description="Name of group to remove from",
        opt_type=OptionType.STRING,
        required=True,
    )
    @check(admin_or_permissions(Permissions.MANAGE_GUILD))
    async def _rolegiver_remove(self, ctx: InteractionContext, group: str) -> None:
        rolegiver = await Rolegiver.find_one(
            Rolegiver.guild == ctx.guild.id, Rolegiver.group == group
        )
        if not rolegiver or (rolegiver and not rolegiver.roles):
            await ctx.send(f"Rolegiver {group} has no roles", ephemeral=True)
            return

        roles: dict[int, Role] = {}

        for role_id in rolegiver.roles:
            role = await ctx.guild.fetch_role(role_id)
            if role:
                roles[int(role.id)] = role

        rolegiver.roles = [r for r in roles.keys()]

        options = [
            StringSelectOption(label=v.name, value=str(k)) for k, v in roles.items()
        ][:25]
        select = StringSelectMenu(
            *options,
            placeholder="Select roles to remove",
            min_values=1,
            max_values=len(options),
        )
        components = [ActionRow(select)]

        message = await ctx.send(
            content=f"Removing roles from {group}", components=components
        )

        try:
            resp = await self.bot.wait_for_component(
                check=lambda x: ctx.author.id == x.ctx.author.id,
                messages=message,
                timeout=60 * 5,
            )

        except asyncio.TimeoutError:
            for row in components:
                for component in row.components:
                    component.disabled = True
            await message.edit(components=components)

        removed_roles = []
        for role_id in resp.ctx.values:
            role = roles.get(int(role_id))
            removed_roles.append(role)
            rolegiver.roles.remove(int(role_id))

        await rolegiver.save()

        fields = [
            EmbedField(
                name="Removed Role(s)",
                value="\n".join(x.mention for x in removed_roles),
            ),
            EmbedField(
                name="Remaining Role(s)",
                value="\n".join(
                    x.mention for x in roles.values() if x not in removed_roles
                ),
            ),
        ]

        embed = build_embed(
            title="Rolegiver Updated",
            description="Role(s) removed from rolegiver",
            fields=fields,
        )
        embed.set_thumbnail(url=ctx.guild.icon.url)
        embed.set_footer(text=f"{ctx.author.username} | {ctx.author.id}")

        await ctx.send(
            embeds=embed,
        )

        self._group_cache[ctx.guild.id].update(
            {group: {v.name: k for k, v in roles.items() if v not in removed_roles}}
        )

    @rolegiver.subcommand(
        sub_cmd_name="list", sub_cmd_description="List rolegiver roles"
    )
    @slash_option(
        name="group",
        description="Name of group to list",
        opt_type=OptionType.STRING,
        required=False,
        autocomplete=True,
    )
    async def _rolegiver_list(self, ctx: InteractionContext, group: str = None) -> None:
        setting = await Rolegiver.find_one(
            Rolegiver.guild == ctx.guild.id, Rolegiver.group == group
        )
        if not setting or (setting and not setting.roles):
            await ctx.send("Rolegiver has no roles", ephemeral=True)
            return

        roles = []
        for role_id in setting.roles:
            e_role = await ctx.guild.fetch_role(role_id)
            if not e_role:
                continue
            roles.append(e_role)

        if roles:
            roles.sort(key=lambda x: -x.position)

        value = "\n".join([r.mention for r in roles]) if roles else "None"

        embed = build_embed(
            title="Rolegiver",
            description=f"Available roles:\n{value}",
            fields=[],
        )

        embed.set_thumbnail(url=ctx.guild.icon.url)

        embed.set_footer(text=f"{ctx.author.username} | {ctx.author.id}")
        components = Button(
            style=ButtonStyle.DANGER, emoji="🗑️", custom_id=f"delete|{ctx.author.id}"
        )
        await ctx.send(embeds=embed, components=components)

    role = SlashCommand(name="role", description="Get roles!")

    @role.subcommand(sub_cmd_name="get", sub_cmd_description="Get a role")
    @slash_option(
        name="group",
        description="Name of group to list",
        opt_type=OptionType.STRING,
        required=False,
        autocomplete=True,
    )
    @cooldown(bucket=Buckets.USER, rate=1, interval=10)
    async def _role_get(self, ctx: InteractionContext, group: str = "Default") -> None:
        rolegiver = await Rolegiver.find_one(
            Rolegiver.guild == ctx.guild.id, Rolegiver.group == group
        )
        if not rolegiver or (rolegiver and not rolegiver.roles):
            await ctx.send(f"Rolegiver group {group} has no roles", ephemeral=True)
            return

        options = []
        roles = []
        for role_id in rolegiver.roles:
            role: Role = await ctx.guild.fetch_role(role_id)
            if not role:
                rolegiver.roles.remove(role_id)
                continue
            roles.append(role)
            if ctx.author.has_role(role):
                continue
            option = StringSelectOption(label=role.name, value=str(role.id))
            options.append(option)

        await rolegiver.save()
        self._group_cache[ctx.guild.id].update({group: {r.name: r.id for r in roles}})

        if len(options) == 0:
            await ctx.send("You have every role from this group!", ephemeral=True)
            return

        select = StringSelectMenu(
            *options,
            placeholder="Select roles to add",
            min_values=1,
            max_values=len(options),
        )
        components = [ActionRow(select)]

        message = await ctx.send(content="\u200b", components=components)

        try:
            response = await self.bot.wait_for_component(
                check=lambda x: ctx.author.id == x.ctx.author.id,
                messages=message,
                timeout=60 * 5,
            )

            added_roles = []
            for role in response.ctx.values:
                role = await ctx.guild.fetch_role(int(role))
                added_roles.append(role)
                await ctx.author.add_role(role, reason="Rolegiver")

            avalue = (
                "\n".join([r.mention for r in added_roles]) if added_roles else "None"
            )
            fields = [
                EmbedField(name="Added Role(s)", value=avalue),
            ]

            embed = build_embed(
                title="User Given Role",
                description=f"{len(added_roles)} role(s) given to {ctx.author.mention}",
                fields=fields,
            )

            embed.set_thumbnail(url=ctx.guild.icon.url)
            embed.set_author(
                name=ctx.author.display_name,
                icon_url=ctx.author.display_avatar.url,
            )

            embed.set_footer(text=f"{ctx.author.username} | {ctx.author.id}")

            for row in components:
                for component in row.components:
                    component.disabled = True

            await response.ctx.edit_origin(
                embeds=embed, content="\u200b", components=components
            )
        except asyncio.TimeoutError:
            for row in components:
                for component in row.components:
                    component.disabled = True
            await message.edit(components=components)

    @role.subcommand(sub_cmd_name="forfeit", sub_cmd_description="Forfeit a role")
    @slash_option(
        name="group",
        description="Name of group to give up from",
        opt_type=OptionType.STRING,
        required=False,
        autocomplete=True,
    )
    @cooldown(bucket=Buckets.USER, rate=1, interval=10)
    async def _role_remove(
        self, ctx: InteractionContext, group: str = "Default"
    ) -> None:
        user_roles = ctx.author.roles

        rolegiver = await Rolegiver.find_one(
            Rolegiver.guild == ctx.guild.id, Rolegiver.group == group
        )
        if not rolegiver or (rolegiver and not rolegiver.roles):
            await ctx.send("Rolegiver has no roles", ephemeral=True)
            return
        elif not any(x.id in rolegiver.roles for x in user_roles):
            await ctx.send(
                f"You have no rolegiver roles from group {group}", ephemeral=True
            )
            return

        valid = list(filter(lambda x: x.id in rolegiver.roles, user_roles))
        options = []
        for role in valid:
            option = StringSelectOption(label=role.name, value=str(role.id))
            options.append(option)

        select = StringSelectMenu(
            *options,
            custom_id="to_remove",
            placeholder="Select roles to remove",
            min_values=1,
            max_values=len(options),
        )
        components = [ActionRow(select)]

        message = await ctx.send(content="\u200b", components=components)

        try:
            context = await self.bot.wait_for_component(
                check=lambda x: ctx.author.id == x.ctx.author.id,
                messages=message,
                timeout=60 * 5,
            )

            removed_roles = []
            for to_remove in context.ctx.values:
                role = get(user_roles, id=int(to_remove))
                await ctx.author.remove_role(role, reason="Rolegiver")
                user_roles.remove(role)
                removed_roles.append(role)

            rvalue = (
                "\n".join([r.mention for r in removed_roles])
                if removed_roles
                else "None"
            )
            fields = [
                EmbedField(name="Removed Role(s)", value=rvalue),
            ]

            embed = build_embed(
                title="User Forfeited Role",
                description=f"{len(removed_roles)} role(s) removed from {ctx.author.mention}",
                fields=fields,
            )

            embed.set_thumbnail(url=ctx.guild.icon.url)
            embed.set_author(
                name=ctx.author.display_name, icon_url=ctx.author.display_avatar.url
            )

            embed.set_footer(text=f"{ctx.author.username} | {ctx.author.id}")

            for row in components:
                for component in row.components:
                    component.disabled = True

            await context.ctx.edit_origin(
                embeds=embed, components=components, content="\u200b"
            )

        except asyncio.TimeoutError:
            for row in components:
                for component in row.components:
                    component.disabled = True
            await message.edit(components=components)

    @rolegiver.subcommand(
        sub_cmd_name="cleanup",
        sub_cmd_description="Removed deleted roles from rolegivers",
    )
    @check(admin_or_permissions(Permissions.MANAGE_GUILD))
    async def _rolegiver_cleanup(self, ctx: InteractionContext) -> None:
        await ctx.defer()
        async for rolegiver in Rolegiver.find(Rolegiver.guild == ctx.guild.id):
            if not rolegiver.roles:
                del self._group_cache[ctx.guild.id][rolegiver.group]
                await rolegiver.delete()
                continue
            guild_role_ids = [r.id for r in ctx.guild.roles]
            for role_id in rolegiver.roles:
                if role_id not in guild_role_ids:
                    rolegiver.roles.remove(role_id)
            await rolegiver.save()
        await ctx.send("Rolegiver cleanup finished")

    @_group_delete.autocomplete("group")
    @_role_get.autocomplete("group")
    @_role_remove.autocomplete("group")
    @_rolegiver_add.autocomplete("group")
    @_rolegiver_remove.autocomplete("group")
    @_rolegiver_list.autocomplete("group")
    async def _autocomplete_group(self, ctx: AutocompleteContext):
        groups = list(self._group_cache.get(ctx.guild.id).keys())
        if not groups:
            rolegivers = await Rolegiver.find(Rolegiver.guild == ctx.guild.id).to_list()
            groups = [r.group for r in rolegivers]

        results = process.extract(ctx.input_text, groups, limit=5)
        choices = [{"name": r[0], "value": r[0]} for r in results]
        await ctx.send(choices)


def setup(bot: Client) -> None:
    """Add RolegiverCog to JARVIS"""
    RolegiverCog(bot)
