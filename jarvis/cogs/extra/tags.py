"""JARVIS Tags Cog."""
import asyncio
import re
from datetime import datetime, timezone
from typing import Dict, List

from interactions import (
    AutocompleteContext,
    Client,
    Extension,
    InteractionContext,
    SlashContext,
)
from interactions.models.discord.components import Button
from interactions.models.discord.embed import EmbedField
from interactions.models.discord.enums import ButtonStyle, Permissions
from interactions.models.discord.modal import InputText, Modal, TextStyles
from interactions.models.internal.application_commands import (
    OptionType,
    SlashCommand,
    slash_option,
)
from jarvis_core.db.models import Setting, Tag
from thefuzz import process

from jarvis.utils import build_embed

invites = re.compile(
    r"(?:https?://)?(?:www.)?(?:discord.(?:gg|io|me|li)|discord(?:app)?.com/invite)/([^\s/]+?)(?=\b)",  # noqa: E501
    flags=re.IGNORECASE,
)
tag_name = re.compile(r"^[\w\ \-]{1,40}$")


class TagCog(Extension):
    def __init__(self, bot: Client):
        self.bot = bot
        self.cache: Dict[int, List[str]] = {}

    tag = SlashCommand(name="tag", description="Create and manage custom tags")

    @tag.subcommand(sub_cmd_name="get", sub_cmd_description="Get a tag")
    @slash_option(
        name="name",
        description="Tag to get",
        autocomplete=True,
        opt_type=OptionType.STRING,
        required=True,
    )
    async def _get(self, ctx: InteractionContext, name: str) -> None:
        tag = await Tag.find_one(Tag.guild == ctx.guild.id, Tag.name == name)
        if not tag:
            await ctx.send(
                "Well this is awkward, looks like the tag was deleted just now",
                ephemeral=True,
            )
            return

        await ctx.send(tag.content)

    @tag.subcommand(sub_cmd_name="create", sub_cmd_description="Create a tag")
    async def _create(self, ctx: SlashContext) -> None:
        modal = Modal(
            *[
                InputText(
                    label="Tag name",
                    placeholder="name",
                    style=TextStyles.SHORT,
                    custom_id="name",
                    max_length=40,
                ),
                InputText(
                    label="Content",
                    placeholder="Content to send here",
                    style=TextStyles.PARAGRAPH,
                    custom_id="content",
                    max_length=1024,
                ),
            ],
            title="Create a new tag!",
        )

        await ctx.send_modal(modal)
        try:
            response = await self.bot.wait_for_modal(
                modal, author=ctx.author.id, timeout=60 * 5
            )
            name = response.responses.get("name").replace("`", "")
            content = response.responses.get("content")
        except asyncio.TimeoutError:
            return

        noinvite = await Setting.find_one(
            Setting.guild == ctx.guild.id, Setting.setting == "noinvite"
        )

        if (
            (invites.search(content) or invites.search(name))
            and noinvite.value
            and not (
                ctx.author.has_permission(Permissions.ADMINISTRATOR)
                or ctx.author.has_permission(Permissions.MANAGE_MESSAGES)
            )
        ):
            await response.send(
                "Listen, don't use this to try and bypass the rules", ephemeral=True
            )
            return
        elif not content.strip() or not name.strip():
            await response.send("Content and name required", ephemeral=True)
            return
        elif not tag_name.match(name):
            await response.send(
                "Tag name must only contain: [A-Za-z0-9_- ]", ephemeral=True
            )
            return

        tag = await Tag.find_one(Tag.guild == ctx.guild.id, Tag.name == name)
        if tag:
            await response.send("That tag already exists", ephemeral=True)
            return

        content = re.sub(r"\\?([@<])", r"\\\g<1>", content)

        tag = Tag(
            creator=ctx.author.id,
            name=name,
            content=content,
            guild=ctx.guild.id,
        )
        await tag.save()

        embed = build_embed(
            title="Tag Created",
            description=f"{ctx.author.mention} created a new tag",
            fields=[
                EmbedField(name="Name", value=name),
                EmbedField(name="Content", value=content),
            ],
        )

        embed.set_author(
            name=ctx.author.username + "#" + ctx.author.discriminator,
            icon_url=ctx.author.display_avatar.url,
        )

        components = Button(
            style=ButtonStyle.DANGER, emoji="🗑️", custom_id=f"delete|{ctx.author.id}"
        )

        await response.send(embeds=embed, components=components)
        if ctx.guild.id not in self.cache:
            self.cache[ctx.guild.id] = []
        self.cache[ctx.guild.id].append(tag.name)

    @tag.subcommand(sub_cmd_name="edit", sub_cmd_description="Edit a tag")
    @slash_option(
        name="name",
        description="Tag name",
        opt_type=OptionType.STRING,
        autocomplete=True,
        required=True,
    )
    async def _edit(self, ctx: InteractionContext, name: str) -> None:
        old_name = name
        tag = await Tag.find_one(Tag.guild == ctx.guild.id, Tag.name == name)
        if not tag:
            await ctx.send("Tag not found", ephemeral=True)
            return
        elif tag.creator != ctx.author.id and not (
            ctx.author.has_permission(Permissions.ADMINISTRATOR)
            or ctx.author.has_permission(Permissions.MANAGE_MESSAGES)
        ):
            await ctx.send(
                "You didn't create this tag, ask the creator to edit it", ephemeral=True
            )
            return

        modal = Modal(
            *[
                InputText(
                    label="Tag name",
                    value=tag.name,
                    style=TextStyles.SHORT,
                    custom_id="name",
                    max_length=40,
                ),
                InputText(
                    label="Content",
                    value=tag.content,
                    style=TextStyles.PARAGRAPH,
                    custom_id="content",
                    max_length=1024,
                ),
            ],
            title="Edit a tag!",
        )

        await ctx.send_modal(modal)
        try:
            response = await self.bot.wait_for_modal(
                modal, author=ctx.author.id, timeout=60 * 5
            )
            name = response.responses.get("name").replace("`", "")
            content = response.responses.get("content")
        except asyncio.TimeoutError:
            return

        new_tag = await Tag.find_one(Tag.guild == ctx.guild.id, Tag.name == name)
        if new_tag and new_tag.id != tag.id:
            await ctx.send(
                "That tag name is used by another tag, choose another name",
                ephemeral=True,
            )
            return

        noinvite = await Setting.find_one(
            Setting.guild == ctx.guild.id, Setting.setting == "noinvite"
        )

        if (
            (invites.search(content) or invites.search(name))
            and noinvite.value
            and not (
                ctx.author.has_permission(Permissions.ADMINISTRATOR)
                or ctx.author.has_permission(Permissions.MANAGE_MESSAGES)
            )
        ):
            await response.send(
                "Listen, don't use this to try and bypass the rules", ephemeral=True
            )
            return
        elif not content.strip() or not name.strip():
            await response.send("Content and name required", ephemeral=True)
            return
        elif not tag_name.match(name):
            await response.send(
                "Tag name must only contain: [A-Za-z0-9_- ]", ephemeral=True
            )
            return

        tag.content = re.sub(r"\\?([@<])", r"\\\g<1>", content)
        tag.name = name
        tag.edited_at = datetime.now(tz=timezone.utc)
        tag.editor = ctx.author.id

        await tag.save()

        embed = build_embed(
            title="Tag Updated",
            description=f"{ctx.author.mention} updated a tag",
            fields=[
                EmbedField(name="Name", value=name),
                EmbedField(name="Content", value=tag.content),
            ],
        )

        embed.set_author(
            name=ctx.author.username + "#" + ctx.author.discriminator,
            icon_url=ctx.author.display_avatar.url,
        )
        components = Button(
            style=ButtonStyle.DANGER, emoji="🗑️", custom_id=f"delete|{ctx.author.id}"
        )
        await response.send(embeds=embed, components=components)
        if tag.name not in self.cache[ctx.guild.id]:
            self.cache[ctx.guild.id].remove(old_name)
            self.cache[ctx.guild.id].append(tag.name)

    @tag.subcommand(sub_cmd_name="delete", sub_cmd_description="Delete a tag")
    @slash_option(
        name="name",
        description="Tag name",
        opt_type=OptionType.STRING,
        required=True,
        autocomplete=True,
    )
    async def _delete(self, ctx: InteractionContext, name: str) -> None:
        tag = await Tag.find_one(Tag.guild == ctx.guild.id, Tag.name == name)
        if not tag:
            await ctx.send("Tag not found", ephemeral=True)
            return
        elif tag.creator != ctx.author.id and not (
            ctx.author.has_permission(Permissions.ADMINISTRATOR)
            or ctx.author.has_permission(Permissions.MANAGE_MESSAGES)
        ):
            await ctx.send(
                "You didn't create this tag, ask the creator to delete it",
                ephemeral=True,
            )
            return

        await tag.delete()
        await ctx.send(f"Tag `{name}` deleted")
        self.cache[ctx.guild.id].remove(tag.name)

    @tag.subcommand(sub_cmd_name="info", sub_cmd_description="Get info on a tag")
    @slash_option(
        name="name",
        description="Tag name",
        opt_type=OptionType.STRING,
        required=True,
        autocomplete=True,
    )
    async def _info(self, ctx: InteractionContext, name: str) -> None:
        tag = await Tag.find_one(Tag.guild == ctx.guild.id, Tag.name == name)
        if not tag:
            await ctx.send("Tag not found", ephemeral=True)
            return

        username, discrim, url, mention = None, None, None, "Unknown User"
        author = await self.bot.fetch_user(tag.creator)
        if author:
            username = author.username
            discrim = author.discriminator
            url = author.display_avatar.url
            mention = author.mention

        ts = int(tag.created_at.timestamp())

        embed = build_embed(
            title="Tag Info",
            description=f"Here's the info on the tag `{name}`",
            fields=[
                EmbedField(name="Name", value=name),
                EmbedField(name="Content", value=tag.content),
                EmbedField(name="Created At", value=f"<t:{ts}:F>"),
                EmbedField(name="Created By", value=mention),
            ],
        )
        if tag.edited_at:
            ets = int(tag.edited_at.timestamp())
            editor = await self.bot.fetch_user(tag.editor)
            emention = "Unknown User"
            if editor:
                emention = editor.mention
            embed.add_field(name="Edited At", value=f"<t:{ets}:F>")
            embed.add_field(name="Edited By", value=emention)

        embed.set_author(
            name=f"{username}#{discrim}" if username else "Unknown User",
            icon_url=url,
        )
        components = Button(
            style=ButtonStyle.DANGER, emoji="🗑️", custom_id=f"delete|{ctx.author.id}"
        )
        await ctx.send(embeds=embed, components=components)

    @tag.subcommand(sub_cmd_name="list", sub_cmd_description="List tag names")
    async def _list(self, ctx: InteractionContext) -> None:
        tags = await Tag.find(Tag.guild == ctx.guild.id).to_list()
        names = "\n".join(f"`{t.name}`" for t in tags)
        embed = build_embed(title="All Tags", description=names, fields=[])
        components = Button(
            style=ButtonStyle.DANGER, emoji="🗑️", custom_id=f"delete|{ctx.author.id}"
        )
        await ctx.send(embeds=embed, components=components)

    @_get.autocomplete("name")
    @_edit.autocomplete("name")
    @_delete.autocomplete("name")
    @_info.autocomplete("name")
    async def _autocomplete(self, ctx: AutocompleteContext) -> None:
        if not self.cache.get(ctx.guild.id):
            tags = await Tag.find(Tag.guild == ctx.guild.id).to_list()
            self.cache[ctx.guild.id] = [tag.name for tag in tags]
        results = process.extract(
            ctx.input_text, self.cache.get(ctx.guild.id), limit=25
        )
        choices = [{"name": r[0], "value": r[0]} for r in results]
        await ctx.send(choices=choices)


def setup(bot: Client) -> None:
    """Add TagCog to JARVIS"""
    TagCog(bot)
