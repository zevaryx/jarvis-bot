"""JARVIS extra, optional cogs"""
from interactions import Client

from jarvis.cogs.extra import calc, dev, image, pinboard, rolegiver, tags


def setup(bot: Client) -> None:
    """
    Add extra cogs to JARVIS

    TODO: load which cogs are subscribed to where and dynamically register them
    to only the guilds that are subscribed to them.
    """
    calc.setup(bot)
    dev.setup(bot)
    image.setup(bot)
    pinboard.setup(bot)
    rolegiver.setup(bot)
    tags.setup(bot)
