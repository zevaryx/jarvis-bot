"""JARVIS guild-specific cogs"""
from interactions import Client

from jarvis.cogs.unique import ctc2, dbrand, gl


def setup(bot: Client) -> None:
    """Add guild-specific cogs to JARVIS"""
    ctc2.setup(bot)
    dbrand.setup(bot)
    gl.setup(bot)
