"""Main JARVIS package."""
import logging
from functools import partial
from typing import Any

import jurigged
from interactions import Intents
from jarvis_core.db import connect
from jarvis_core.log import get_logger
from redis import asyncio as aioredis
from statipy.db import Stat, StaticStat

from jarvis import const
from jarvis.client import Jarvis
from jarvis.cogs import __path__ as cogs_path
from jarvis.config import load_config
from jarvis.utils import get_extensions

__version__ = const.__version__


def jlogger(logger: logging.Logger, event: Any) -> None:
    """
    Logging for jurigged

    Args:
        logger: Logger to use
        event: Event to parse
    """
    jlog = partial(logger.log, 11)
    if isinstance(event, jurigged.live.WatchOperation):
        jlog(f"[bold]Watch[/] {event.filename}", extra={"markup": True})
    elif isinstance(event, jurigged.codetools.AddOperation):
        event_str = f"{event.defn.parent.dotpath()}:{event.defn.stashed.lineno}"
        if isinstance(event.defn, jurigged.codetools.LineDefinition):
            event_str += f" | {event.defn.text}"
            jlog(
                f"[bold green]Run[/] {event_str}",
                extra={"markup": True},
            )
        else:
            jlog(f"[bold green]Add[/] {event_str}", extra={"markup": True})
    elif isinstance(event, jurigged.codetools.UpdateOperation):
        if isinstance(event.defn, jurigged.codetools.FunctionDefinition):
            event_str = f"{event.defn.parent.dotpath()}:{event.defn.stashed.lineno}"
            jlog(f"[bold yellow]Update[/] {event_str}", extra={"markup": True})
    elif isinstance(event, jurigged.codetools.DeleteOperation):
        event_str = f"{event.defn.parent.dotpath()}:{event.defn.stashed.lineno}"
        if isinstance(event.defn, jurigged.codetools.LineDefinition):
            event_str += f" | {event.defn.text}"
        jlog(f"[bold red]Delete[/] {event_str}", extra={"markup": True})
    elif isinstance(event, (Exception, SyntaxError)):
        logger.exception("Jurigged encountered error", exc_info=True)
    else:
        jlog(event)


async def run() -> None:
    """Run JARVIS"""
    # Configure logger
    config = load_config()
    logger = get_logger("jarvis", show_locals=False)  # jconfig.log_level == "DEBUG")
    logger.setLevel(config.log_level)
    file_handler = logging.FileHandler(
        filename="jarvis.log", encoding="UTF-8", mode="w"
    )
    file_handler.setFormatter(
        logging.Formatter("[%(asctime)s] [%(name)s] [%(levelname)8s] %(message)s")
    )
    logger.addHandler(file_handler)

    # Configure client
    intents = (
        Intents.DEFAULT
        | Intents.MESSAGES
        | Intents.GUILD_MEMBERS
        | Intents.GUILD_MESSAGES
        | Intents.MESSAGE_CONTENT
    )
    redis_config = config.redis.model_dump()
    redis_host = redis_config.pop("host")

    redis = await aioredis.from_url(redis_host, decode_responses=True, **redis_config)

    await connect(
        **config.mongo.model_dump(),
        testing=config.environment.value == "develop",
        extra_models=[StaticStat, Stat],
    )

    jarvis = Jarvis(
        intents=intents,
        sync_interactions=config.sync,
        delete_unused_application_cmds=True,
        send_command_tracebacks=False,
        redis=redis,
        logger=logger,
        erapi=config.erapi,
    )

    # External modules
    if config.jurigged:
        logging.addLevelName(11, "\033[35mJURIG\033[0m   ")
        if config.log_level == "INFO":
            logger.setLevel(11)
        jurigged.watch(pattern="jarvis/*.py", logger=partial(jlogger, logger))

    # Initialize bot
    logger.info("Starting JARVIS")
    logger.debug("Connecting to database")
    logger.debug("Loading configuration from database")
    # jconfig.get_db_config()

    # Load extensions
    logger.debug("Loading extensions")
    for extension in get_extensions(cogs_path):
        jarvis.load_extension(extension)
        logger.debug("Loaded %s", extension)

    logger.debug("Loading statipy")
    jarvis.load_extension("statipy.ext")

    logger.debug("Running JARVIS")
    await jarvis.astart(config.token)
