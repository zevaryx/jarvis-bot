"""Unicode emoji data."""
import json
from os import getcwd
from os import sep as s

json_path = getcwd()
if "jarvis" != json_path.split(s)[-1]:
    json_path += s + "jarvis"
json_path += f"{s}data{s}json{s}"

with open(json_path + "emoji_list.json", encoding="UTF8") as f:
    emoji_list = json.load(f)

with open(json_path + "emoji_data.json", encoding="UTF8") as f:
    emoji_data = json.load(f)
