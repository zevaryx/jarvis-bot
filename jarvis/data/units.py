"""
Unit Converter utilities.

Rates based on https://github.com/microsoft/calculator
"""
from dataclasses import dataclass, field


@dataclass
class Converter:
    BASE: str = "sample"
    CONVERSIONS: dict[str, int] = field(default_factory=dict)

    @classmethod
    def get_rate(cls, from_: str, to: str) -> int:
        """
        Get the conversion rate

        Args:
            from_: Convert from this
            to: Convert to this
        """
        if from_ == cls.BASE:
            return cls.CONVERSIONS.get(to, None)
        else:
            from_rate = cls.CONVERSIONS.get(from_)
            to_rate = cls.CONVERSIONS.get(to)
            return (1 / from_rate) * to_rate


class Angle(Converter):
    BASE = "degree"
    CONVERSIONS = {"degree": 1, "radian": 57.29577951308233, "gradian": 0.9}


class Area(Converter):
    BASE = "square meter"
    CONVERSIONS = {
        "acre": 4046.8564224,
        "square meter": 1,
        "square foot": 0.09290304,
        "square yard": 0.83612736,
        "square millimeter": 0.000001,
        "square centimeter": 0.00001,
        "square inch": 0.00064516,
        "square mile": 2589988.110336,
        "square kilometer": 1000000,
        "hectare": 10000,
        "hand": 0.01251604,
        "paper": 0.06032246,
        "soccer field": 10869.66,
        "castle": 100000,
        "pyeong": 400 / 121,
    }


class Data(Converter):
    BASE = "megabyte"
    CONVERSIONS = {
        "bit": 0.000000125,
        "byte": 0.000001,
        "kilobyte": 0.001,
        "megabyte": 1,
        "gigabyte": 1000,
        "terabyte": 1000000,
        "petabyte": 1000000000,
        "exabyte": 1000000000000,
        "zetabyte": 1000000000000000,
        "yottabyte": 1000000000000000000,
        "kilobit": 0.000125,
        "megabit": 0.0125,
        "gigabit": 125,
        "terabit": 125000,
        "petabit": 125000000,
        "exabit": 125000000000,
        "zetabit": 125000000000000,
        "yottabit": 125000000000000000,
        "gibibit": 134.217728,
        "gibibyte": 1073.731824,
        "kibibit": 0.000128,
        "kibibyte": 0.001024,
        "mebibit": 0.131072,
        "mebibyte": 1.048576,
        "pebibit": 140737488.355328,
        "pebibyte": 1125899906.842624,
        "tebibit": 137438.953472,
        "tebibyte": 1099511.627776,
        "exbibit": 144115188075.855872,
        "exbibyte": 1152921504606.846976,
        "zebibit": 147573952589676.412928,
        "zebibyte": 1180591620717411.303424,
        "yobibit": 151115727451828646.838272,
        "yobibyte": 1208925819614629174.706176,
        "floppy disk": 1.474560,
        "cd": 734.003200,
        "dvd": 5046.586573,
    }


class Energy(Converter):
    BASE = "joule"
    CONVERSIONS = {
        "calorie": 4.184,
        "kilocalorie": 4184,
        "british thermal unit": 1055.056,
        "kilojoule": 1000,
        "electron volt": 0.0000000000000000001602176565,
        "joule": 1,
        "foot pound": 1.3558179483314,
        "battery": 9000,
        "banana": 439614,
        "slice of cake": 1046700,
    }


class Length(Converter):
    BASE = "meter"
    CONVERSIONS = {
        "inch": 0.0254,
        "foot": 0.3048,
        "yard": 0.9144,
        "mile": 1609.344,
        "micron": 0.000001,
        "millimeter": 0.001,
        "nanometer": 0.000000001,
        "centimeter": 0.01,
        "meter": 1,
        "kilometer": 1000,
        "nautical mile": 1852,
        "paperclip": 0.035052,
        "hand": 0.18669,
        "jumbo jet": 76,
    }


class Power(Converter):
    BASE = "watt"
    CONVERSIONS = {
        "british thermal unit per minute": 17.58426666666667,
        "foot pound per minute": 0.0225969658055233,
        "watt": 1,
        "kilowatt": 1000,
        "horsepower": 745.69987158227022,
        "lightbulb": 60,
        "horse": 745.7,
        "train engine": 2982799.486329081,
    }


class Pressure(Converter):
    BASE = "atmosphere"
    CONVERSIONS = {
        "atmosphere": 1,
        "bar": 0.9869232667160128,
        "kilopascal": 0.0098692326671601,
        "millimeter of mercury": 0.0013155687145324,
        "pascal": 9.869232667160128e-6,
        "psi": 0.068045961016531,
    }


class Speed(Converter):
    BASE = "centimeters per second"
    CONVERSIONS = {
        "centimeters per second": 1,
        "feet per second": 30.48,
        "kilometers per hour": 27.777777777777777777778,
        "knot": 51.44,
        "mach": 34040,
        "meters per second": 100,
        "miles per hour": 44.7,
        "turtle": 8.94,
        "horse": 2011.5,
        "jet": 24585,
    }


class Time(Converter):
    BASE = "second"
    CONVERSIONS = {
        "day": 86400,
        "second": 1,
        "week": 604800,
        "millisecond": 0.001,
        "microsecond": 0.000001,
        "minute": 60,
        "hour": 3600,
    }


class Volume(Converter):
    BASE = "millileter"
    CONVERSIONS = {
        "cup (US)": 236.588237,
        "pint (US)": 473.176473,
        "pint (UK)": 568.26125,
        "quart (US)": 946.352946,
        "quart (UK)": 1136.5225,
        "gallon (US)": 3785.411784,
        "gallon (UK)": 4546.09,
        "liter": 1000,
        "teaspoon (US)": 4.92892159375,
        "tablespoon (US)": 14.78676478125,
        "cubic centimeter": 1,
        "cubic yard": 764554.857984,
        "cubic meter": 1000000,
        "millimeter": 1,
        "cubic inch": 16.387064,
        "cubic foot": 28316.846592,
        "fluid ounce (US)": 29.5735295625,
        "fluid ounce (UK)": 28.4130625,
        "teaspoon (UK)": 5.91938802083333333333,
        "tablespoon (UK)": 17.7581640625,
        "coffee cup": 236.588237,
        "bathtub": 378541.2,
        "swimming pool": 3750000000,
    }


class Weight(Converter):
    BASE = "kilogram"
    CONVERSIONS = {
        "kilogram": 1,
        "hectogram": 0.1,
        "decagram": 0.01,
        "gram": 0.001,
        "pound": 0.45359237,
        "ounce": 0.028349523125,
        "milligram": 0.000001,
        "centigram": 0.00001,
        "decigram": 0.0001,
        "long ton": 1016.0469088,
        "tonne": 1000,
        "stone": 6.35029318,
        "carat": 0.0002,
        "short ton": 907.18474,
        "snowflake": 0.000002,
        "soccer ball": 0.4325,
        "elephant": 4000,
        "whale": 90000,
    }
