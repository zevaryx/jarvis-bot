#!/bin/bash
if [ "$EUID" -ne 0 ]; then
  sudo $0
  exit 0
fi
echo Installing requirements...
if [ -f /etc/os-release ]; then
  . /etc/os-release
  VER=$VERSION_ID
  ID=$ID
elif type lsb_release >/dev/null 2>&1; then
  OS=$(lsb_release -si)
  VER=$(lsb_release -sr)
  ID=$ID
else
  echo WARNING: Unknown OS. Please install tmux by hand.
  echo This script may error during install.
fi

if [ "$ID" -eq "ubuntu" ] || [ "$ID" -eq "debian" ]; then
  apt update
  apt install tmux python3 python3-pip -y
elif [ "$ID" -eq "arch" ] || [ "$ID" -eq "manjaro" ]; then
  pacman -Sy tmux python python-pip
fi

curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
cargo install tokei --features all

if [ ! -d "/home/jarvis" ]; then
  echo Creating user jarvis...
  useradd -m jarvis
fi

sudo -u jarvis pip3 install -r requirements.txt

if [ ! -d "/home/jarvis/j.a.r.v.i.s." ]; then
  echo Copying J.A.R.V.I.S. files...
  mkdir -p /home/jarvis/j.a.r.v.i.s.
  cp -r * /home/jarvis/j.a.r.v.i.s./
  chown -r jarvis:jarvis /home/jarvis/j.a.r.v.i.s.
fi

echo Creating ssh config for updates...
sudo -u jarvis mkdir -p /home/jarvis/.ssh
sudo -u jarvis ssh-keygen -q -t ed25519 -n "j.a.r.v.i.s." -f /home/jarvis/.ssh/id_ed25519
cat << 'EOF' >> /home/jarvis/.ssh/config
Host git.zevaryx.com
  HostName git.zevaryx.com
  Port 22
  User git
  IdentityFile /home/jarvis/.ssh/id_ed25519
EOF
chown jarvis:jarvis /home/jarvis/.ssh/config

echo Give this key to your local admin:
cat /home/jarvis/.ssh/id_ed25519.pub

if [ ! -f "/etc/systemd/system/jarvis.service" ]; then
  echo Installing systemd service...
  cp jarvis.service /etc/systemd/system/jarvis.service
  systemctl daemon-reload
fi
echo Enabling J.A.R.V.I.S.
systemctl enable --now jarvis.service

echo J.A.R.V.I.S. should now be online.
echo Connect to console using tmux -a jarvis
